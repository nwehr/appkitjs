<?php
namespace AppKit;

require_once dirname( __FILE__ ) . "/AppKit-TableModel.php"; 

class MySQLTableModel extends TableModel {
	protected $m_Conn; 
	
	public function __construct($host, $user, $pass, $database) {
		parent::__construct( NULL ); 
		
		$this->m_Conn = new \mysqli( $host, $user, $pass, $database ); 
	}
	
	public function __destruct() {}
	
	public function NumRows() {
		
	}
	
	public function Rows( $i_StartIndex = 0, $i_EndIndex = NULL ) {
	}
	
	public function Value( $i_RowIndex, $i_ColumnIndex ) {
	}
	
	public function SetRow( $i_Row, $i_RowIndex ) {
	}
	
	public function SetValue( $i_Value, $i_RowIndex, $i_ColumnIndex ) {
	}
	
	public function InsertRow( $i_Row ) {
	}
	
}
