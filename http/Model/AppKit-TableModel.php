<?php
namespace AppKit;

require_once dirname( __FILE__ ) . "/AppKit-Model.php"; 

class TableModel extends Model {
	public function __construct( $i_Data ) {
		parent::__construct( $i_Data ? $i_Data : array() ); 
	}
	
	public function __destruct() {}

	public function SortCompare( $i_Sort ) {
		return function( $a, $b ) use( $i_Sort ) {
			foreach( $i_Sort as $Key => $Value ) {
				if( $Value == "asc" ) {
					if( $a[$Key] < $b[$Key] )
						return -1; 

					if( $a[$Key] > $b[$Key] )
						return 1; 

				}

				if( $Value == "desc" ) {
					if( $a[$Key] < $b[$Key] )
						return 1; 

					if( $a[$Key] > $b[$Key] )
						return -1; 
					
				}

			}

			return 0; 

		};

	}

	public function RowSatisfiesFilter( $i_Row, $i_Filter ) {
		foreach( $i_Filter as $Key => $Value ) {
			$Subject = is_bool( $i_Row[$Key] ) ? ($i_Row[$Key] ? "true" : "false") : strval( $i_Row[$Key] );

			if( is_object( $Value ) && get_class( $Value ) == "AppKit\RegExp" ) {
				if( !$i_Filter[$Key]->Test( $Subject ) ) {
					return false; 
				}

			} else {
				if( $i_Filter[$Key] != $Subject ) {
					return false; 
				}

			}

		}

		return true; 

	}
	
	public function NumRows( $i_Filter = NULL ) {
		if( $i_Filter ) {
			$Count = 0; 

			for( $i = 0; $i < count( $this->m_Data ); ++$i ) {
				if( $this->RowSatisfiesFilter( $this->m_Data[$i], $i_Filter ) )
					++$Count; 

			}

			return $Count; 

		}

		return count( $this->m_Data ); 

	}
	
	public function Rows( $i_StartIndex = 0, $i_EndIndex = NULL, $i_Filter = NULL, $i_Sort = NULL ) {
		$Rows = array(); 

		if( $i_Filter ) {
			for( $i = 0; $i < count( $this->m_Data ); ++$i ) {
				if( $this->RowSatisfiesFilter( $this->m_Data[$i], $i_Filter ) )
					$Rows[] = &$this->m_Data[$i];

				if( count( $Rows ) == ($i_EndIndex + 1) - $i_StartIndex ) 
					break; 

			}

		} else {
			$Rows =  array_slice( $this->m_Data, $i_StartIndex, $i_EndIndex + 1 ); 
		}

		if( $i_Sort )
			usort( $Rows, $this->SortCompare( $i_Sort ) ); 

		return $Rows; 

	}
	
	public function Value( $i_RowIndex, $i_ColumnIndex, $i_Filter = NULL, $i_Sort = NULL ) {
		if( $i_Filter ) {
			$Rows = array(); 

			for( $i = 0; $i < count( $this->m_Data ); ++$i ) {
				if( $this->RowSatisfiesFilter( $this->m_Data[$i], $i_Filter ) )
					$Rows[] = &$this->m_Data[$i];

				if( count( $Rows ) == $i_EndIndex - $i_StartIndex ) 
					break; 

			}

			return $Rows[$i_RowIndex][$i_ColumnIndex]; 

		}

		return $this->m_Data[$i_RowIndex][$i_ColumnIndex]; 

	}
	
	public function SetValue( $i_Value, $i_RowIndex, $i_ColumnIndex, $i_Filter = NULL, $i_Sort = NULL ) {
		if( $i_Filter ) {
			$Rows = array(); 

			for( $i = 0; $i < count( $this->m_Data ); ++$i ) {
				if( $this->RowSatisfiesFilter( $this->m_Data[$i], $i_Filter ) )
					$Rows[] = &$this->m_Data[$i];

				if( count( $Rows ) == $i_EndIndex - $i_StartIndex ) 
					break; 

			}

			if( $i_Sort )
				usort( $Rows, $this->SortCompare( $i_Sort ) ); 

			$Rows[$i_RowIndex][$i_ColumnIndex] = $i_Value; 

			return true; 

		}

		if( $i_Sort )
			usort( $Rows, $this->SortCompare( $i_Sort ) ); 

		$this->m_Data[$i_RowIndex][$i_ColumnIndex] = $i_Value; 

		return true; 
	}
	
	public function InsertRow( $i_Row ) {
		array_push( $this->m_Data, $i_Row ); 
	}
	
}
