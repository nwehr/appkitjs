<?php
namespace AppKit;

abstract class Model {
	protected $m_Data = NULL; 
	
	public function __construct( $i_Data ) {
		if( $i_Data ) $this->m_Data = $i_Data; 
	}
	
	public function SetData( $i_Data ) {
		$this->m_Data = $i_Data; 
	}
	
	public function Data() {
		return $this->m_Data; 
	}
	
}
