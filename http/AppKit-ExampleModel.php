<?php 

require_once dirname( __FILE__ ) . "/Model/AppKit-TableModel.php"; 
require_once dirname( __FILE__ ) . "/Interface/AppKit-TableModelInterface.php";

$Data = array(
	array( "upper" => "A", "lower" => "a", "vowel" => true )
	, array( "upper" => "F", "lower" => "f", "vowel" => false )
	, array( "upper" => "Z", "lower" => "z", "vowel" => false )
	, array( "upper" => "A", "lower" => "a", "vowel" => true )
	, array( "upper" => "D", "lower" => "d", "vowel" => false )
	, array( "upper" => "E", "lower" => "e", "vowel" => true )
	, array( "upper" => "O", "lower" => "o", "vowel" => true )
); 

$Model = new \AppKit\TableModel( $Data ); 

$Interface = new \AppKit\TableModelInterface( $Model, $_GET ); 
echo $Interface->Respond(); 
