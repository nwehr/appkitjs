<?php
namespace AppKit;

require_once dirname( __FILE__ ) . "/AppKit-ModelInterface.php"; 

class TableModelInterface extends ModelInterface {
	public function __construct( $i_Model, $i_Request ) {
		parent::__construct( $i_Model, $i_Request ); 
	}
	
	public function Respond() {
		$Filter = (array_key_exists( "filter", $this->m_Request ) ? $this->m_Request["filter"] : NULL); 
		$Sort 	= (array_key_exists( "sort", $this->m_Request ) ? $this->m_Request["sort"] : NULL); 

		try {
			switch( $this->m_Request["cmd"] ) {
				case "num_rows":
				{
					$this->m_Response["data"] = $this->m_Model->NumRows( $Filter ); 
				}
				break; 
				case "rows":
				{
					if( !isset( $this->m_Request["start_index"] ) )
						throw new \Exception( "Missing \"start_index\" in request" ); 

					if( !isset( $this->m_Request["end_index"] ) )
						throw new \Exception( "Missing \"end_index\" in request" ); 
					
					$this->m_Response["data"] = $this->m_Model->Rows( $this->m_Request["start_index"], $this->m_Request["end_index"], $Filter, $Sort ); 

				}
				break; 
				case "value":
				{
					if( !isset( $this->m_Request["row_index"] ) )
						throw new \Exception( "Missing \"row_index\" in request" );

					if( !isset( $this->m_Request["col_index"] ) )
						throw new \Exception( "Missing \"col_index\" in request" );

					$this->m_Response["data"] = $this->m_Model->Value( $this->m_Request["row_index"], $this->m_Request["col_index"], $Filter, $Sort ); 

				}
				break; 
				case "set_row":
				{

				}
				break; 
				case "set_value":
				{
					if( !isset( $this->m_Request["value"] ) )
						throw new \Exception( "Missing \"value\" in request" );

					if( !isset( $this->m_Request["row_index"] ) )
						throw new \Exception( "Missing \"row_index\" in request" );

					if( !isset( $this->m_Request["col_index"] ) )
						throw new \Exception( "Missing \"col_index\" in request" );

					if( !$this->m_Model->SetValue( $this->m_Request["value"], $this->m_Request["row_index"], $this->m_Request["col_index"], $Filter, $Sort ) )
						 throw new \Exception( "Unable to set value at [\"" . $this->m_Request["row_index"] . "\"][\"" . $this->m_Request["col_index"] . "\"]" );

				}
				break; 
				case "insert_row":
				{

				}
				break; 
				default:
				{
					throw new \Exception( "Missing \"cmd\" in request" );
				}
				break; 

			}
			
		} catch( \Exception $e ) {
			$this->m_Response["status"] 	= "ERROR"; 
			$this->m_Response["message"] 	= $e->getMessage(); 
		}

		return json_encode( $this->m_Response ); 

	}
	
}