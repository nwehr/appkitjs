<?php
namespace AppKit;

class RegExp {
	protected $m_Pattern; 

	public function __construct( $i_Pattern ) {
		$this->m_Pattern = $i_Pattern; 
	}

	public function Pattern() {
		return $this->m_Pattern; 
	}

	public function Exec( $i_Subject ) {
		$Matches = array(); 

		if( (bool)preg_match( $this->m_Pattern, $i_Subject, $Matches ) )
			return $Matches[0]; 

		return null; 

	}

	public function Test( $i_Subject ) {
		return (bool)preg_match( $this->m_Pattern, $i_Subject ); 
	}

}

class ModelInterface {
	protected $m_Model; 
	protected $m_Request; 
	
	protected $m_Response = array( "status" => "OK" ); 
	
	public function __construct( $i_Model, $i_Request ) {
		$this->m_Model 		= $i_Model; 
		$this->m_Request 	= $i_Request; 

		if( array_key_exists( "event_id", $this->m_Request ) )
			$this->m_Response["event_id"] = $this->m_Request["event_id"]; 

		if( array_key_exists( "filter", $this->m_Request ) ) {
			foreach( $this->m_Request["filter"] as $Key => $Value ) {
				if( substr_compare($Value, "/", 0, 1 ) == 0 
					&& 
					(
						substr_compare($Value, "/", -1, 1 ) 	== 0
						|| substr_compare($Value, "/i", -1, 2 ) == 0
						|| substr_compare($Value, "/m", -1, 2 ) == 0
						|| substr_compare($Value, "/g", -1, 2 ) == 0
					) 
				)
				{
					$this->m_Request["filter"][$Key] = new RegExp( $Value ); 
				}

			}

		}

	}
	
	public function __destruct() {}
	
	public function Respond() {
		return json_encode( $Response ); 
	}
	
}