define(["AppKit/Core/AppKit-Base", "AppKit/Controller/AppKit-Controller"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// TableController : Controller
	///////////////////////////////////////////////////////////////////////////////
	AppKit.TableController = function( i_View, i_Model, i_DataColumnsAsTableColumns ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Controller.apply( this, [i_View, i_Model] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_SelectSlot : {
				value 		: new AppKit.Slot()
				, writable	: true
			}
			, m_DoubleSelectSlot : {
				value 		: new AppKit.Slot()
				, writable 	: true
			}
			, m_ChangeSlot : {
				value 		: new AppKit.Slot()
				, writable 	: true
			}
			, m_ScrollBottomSlot : {
				value		: new AppKit.Slot()
				, writable	: true
			}
			, m_DataColumnsAsTableColumns : {
				value 		: []
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Select = function( i_ViewCell, i_ModelCell ) {}; 
		
		this.__prototype__.HandleSelect = function( i_Args ) {
			var ViewCell = i_Args[1]; 
			var ModelCell = this.m_Model.Value( ViewCell.RowIndex(), this.m_DataColumnsAsTableColumns[ViewCell.ColumnIndex()] ); 
			
			this.Select( ViewCell, ModelCell ); 
			
		}; 
		
		this.__prototype__.DoubleSelect = function( i_ViewCell, i_ModelCell ) {}; 
		
		this.__prototype__.HandleDoubleSelect = function( i_Args ) {
			var ViewCell = i_Args[1]; 
			var ModelCell = this.m_Model.Value( ViewCell.RowIndex(), this.m_DataColumnsAsTableColumns[ViewCell.ColumnIndex()] ); 
			
			this.DoubleSelect( ViewCell, ModelCell ); 
			
		}; 

		this.__prototype__.Change = function( i_ViewCell, i_ModelCell ) {
			this.m_Model.SetValue( i_ViewCell.Value(), i_ViewCell.RowIndex(), this.m_DataColumnsAsTableColumns[i_ViewCell.ColumnIndex()] ); 
		}; 

		this.__prototype__.HandleChange = function( i_Args ) {
			var ViewCell = i_Args[1]; 
			var ModelCell = this.m_Model.Value( ViewCell.RowIndex(), this.m_DataColumnsAsTableColumns[ViewCell.ColumnIndex()] ); 

			this.Change( ViewCell, ModelCell ); 

		}; 
		
		this.__prototype__.RefreshView = function( i_RecursiveEmitRefresh ) {
			this.m_View.Empty(); 

			var NumRows = this.m_Model.NumRows( (i_RecursiveEmitRefresh === false ? false : true) ); 
			
			for( var i = 0; i < NumRows; ++i ) {
				this.m_View.AddRow(); 
				
				for( var n = 0; n < this.m_DataColumnsAsTableColumns.length; ++n ) {
					if( n > this.m_View.NumColumns() - 1 ) {
						this.m_View.AddColumn( "C" + (n + 1) ); 
					}
					
					this.m_View.GetCell( i, n ).SetValue( this.m_Model.Value( i, this.m_DataColumnsAsTableColumns[n] ) ); 
					
				}
				
			}
			
			this.m_View.SetDoEmitScrollBottom( true );
			
		}; 
		
		this.__prototype__.HandleRefreshView = function( i_Args ) {
			this.RefreshView( i_Args[1] ); 
		}; 
		
		this.__prototype__.DataColumnsAsTableColumns = function() {
			return this.m_View.DataColumns(); 
		}
		
		this.__prototype__.SetDataColumnsAsTableColumns = function( i_Columns ) {
			if( !i_Columns || !(i_Columns instanceof Array) ) {
				throw new Error( "parameter i_Columns for AppKit::TableController::SetDataColumnsAsTableColumns() is of incorrect type; expecting Array" ); 
			}
			
			this.m_DataColumnsAsTableColumns = i_Columns; 
			
		}; 
		
		this.__prototype__.HandleScrollBottom = function( i_Args ) {}; 
		
		// Constructor
		{
			if( this.m_View ) {
				this.m_SelectSlot.SetSelector( this.HandleSelect ); 
				this.m_SelectSlot.SetContext( this ); 
			
				this.m_View.SelectSignal().Connect( this.m_SelectSlot ); 
				
				this.m_DoubleSelectSlot.SetSelector( this.HandleDoubleSelect ); 
				this.m_DoubleSelectSlot.SetContext( this ); 
			
				this.m_View.DoubleSelectSignal().Connect( this.m_DoubleSelectSlot ); 
				
				this.m_ChangeSlot.SetSelector( this.HandleChange ); 
				this.m_ChangeSlot.SetContext( this ); 
			
				this.m_View.ChangeSignal().Connect( this.m_ChangeSlot ); 

				this.m_ScrollBottomSlot.SetSelector( this.HandleScrollBottom ); 
				this.m_ScrollBottomSlot.SetContext( this ); 
				
				this.m_View.ScrollBottomSignal().Connect( this.m_ScrollBottomSlot ); 
				
			} 
			
			if( this.m_Model ) {
				this.m_RefreshViewSlot.SetSelector( this.HandleRefreshView ); 
				this.m_RefreshViewSlot.SetContext( this ); 
			
				this.m_Model.RefreshViewSignal().Connect( this.m_RefreshViewSlot );
				
			}
			
			if( i_DataColumnsAsTableColumns ) {
				this.SetDataColumnsAsTableColumns( i_DataColumnsAsTableColumns ); 
			} 
			
		}
		
	}; 
	
	AppKit.TableController.prototype = new AppKit.Controller; 
	
}); 
