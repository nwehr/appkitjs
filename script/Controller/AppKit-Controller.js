define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View", "AppKit/Model/AppKit-Model"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Controller
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Controller = function( i_View, i_Model ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_View : {
				value 		: null
				, writable 	: true
			}
			, m_Model : {
				value 		: null
				, writable	: true
			}
			, m_RefreshViewSlot : {
				value 		: new AppKit.Slot()
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.View = function() {
			return this.m_View; 
		}; 
		
		this.__prototype__.SetView = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.View) ) {
				throw new Error( "parameter i_View for AppKit::Controller::SetView() is of incorrect type; expecting AppKit.View" ); 
			}
			
			this.m_View = i_View; 
			
		}; 
		
		this.__prototype__.Model = function() {
			return this.m_Model; 
		}; 
		
		this.__prototype__.SetModel = function( i_Model ) {
			if( !i_Model || !(i_Model instanceof AppKit.Model) ) {
				throw new Error( "parameter i_Model for AppKit::Controller::SetModel() is of incorrect type; expecting AppKit.Model" ); 
			}
			
			this.m_Model = i_Model; 
			
		}; 
		
		this.__prototype__.RefreshView = function() {
			AppKit.LOG( AppKit.LOG_SEVERITY.WARNING, "AppKit::Controller::RefreshView() must be overridden" ); 
		}; 
		
		this.__prototype__.HandleRefreshView = function( i_Args ) {
			AppKit.LOG( AppKit.LOG_SEVERITY.WARNING, "AppKit::Controller::HandleRefreshView() must be overridden" ); 
		}; 
		
		// Constructor
		{
			if( i_View ) 		this.SetView	( i_View ); 
			if( i_Model ) 		this.SetModel	( i_Model ); 
		}
		
	}; 
		
}); 
