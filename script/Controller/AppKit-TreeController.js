define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-Tree", "AppKit/Model/AppKit-TreeModel", "AppKit/Controller/AppKit-Controller"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// TreeController : Controller
	///////////////////////////////////////////////////////////////////////////////
	AppKit.TreeController = function( i_View, i_Model ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Controller.apply( this, [i_View, i_Model] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_SelectSlot : {
				value		: new AppKit.Slot()
				, writable	: true
			}
			, m_DoubleSelectSlot : {
				value 		: new AppKit.Slot()
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Select = function( i_ViewNode, i_ModelNode ) {}; 
		
		this.__prototype__.HandleSelect = function( i_Args ) {
			this.Select( i_Args[1], this.m_Model.GetNode( i_Args[1].Path() ) ); 
		}; 
		
		this.__prototype__.DoubleSelect = function( i_ViewNode, i_ModelNode ) {}; 
		
		this.__prototype__.HandleDoubleSelect = function( i_Args ) {
			this.DoubleSelect( i_Args[1], this.m_Model.GetNode( i_Args[1].Path() ) ); 
		}; 
		
		this.__prototype__.RefreshView = function( i_ViewRoot, i_ModelRoot ) {
			var NewViewRoot		= i_ViewRoot	? i_ViewRoot	: this.m_View.Root();
			var NewModelRoot	= i_ModelRoot	? i_ModelRoot	: this.m_Model.Data();
			
			NewViewRoot.Empty(); 
			
			for( NewModelRoot = NewModelRoot.Child(); NewModelRoot !== null; NewModelRoot = NewModelRoot.Sibling() ) {
				if( !NewModelRoot.IsLeaf() ) {
					this.RefreshView( NewViewRoot.Append( new AppKit.Branch( NewModelRoot.ValueAtIndex( 0 ) ) ), NewModelRoot ); 
				} else {
					NewViewRoot.Append( new AppKit.Leaf( NewModelRoot.ValueAtIndex( 0 ) ) ); 
				}
				
			}
						
		}; 
		
		this.__prototype__.HandleRefreshView = function( i_Args ) {
			var ModelNode = i_Args[1]; 
			var ViewNode = this.m_View.GetNode( i_Args[1].Path() ); 
			
			var Path = i_Args[1].Path(); 

			while( (!ViewNode || ModelNode === i_Args[1]) && ModelNode.Parent() ) {
				Path.pop(); 

				ViewNode	= this.m_View.GetNode( Path ); 
				ModelNode	= ModelNode.Parent(); 

			}

			this.RefreshView( ViewNode, ModelNode ); 
			
		}; 
		
		// Constructor
		{
			if( this.m_View ) {
				this.m_SelectSlot.SetSelector( this.HandleSelect ); 
				this.m_SelectSlot.SetContext( this ); 
				
				this.m_View.SelectSignal().Connect( this.m_SelectSlot ); 
				
				this.m_DoubleSelectSlot.SetSelector( this.HandleDoubleSelect ); 
				this.m_DoubleSelectSlot.SetContext( this ); 
				
				this.m_View.DoubleSelectSignal().Connect( this.m_DoubleSelectSlot ); 
				
			}
			
			if( this.m_Model ) {
				this.m_RefreshViewSlot.SetSelector( this.HandleRefreshView ); 
				this.m_RefreshViewSlot.SetContext( this ); 
				
				this.m_Model.RefreshViewSignal().Connect( this.m_RefreshViewSlot ); 
			}
			
		}
		
	}; 
	
	AppKit.TreeController.prototype = new AppKit.Controller;
		
}); 
