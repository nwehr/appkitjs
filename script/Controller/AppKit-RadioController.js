define(["AppKit/Core/AppKit-Base", "AppKit/Controller/AppKit-Controller", "AppKit/View/AppKit-Input", "AppKit/Model/AppKit-RadioModel"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// RadioController : Controller
	///////////////////////////////////////////////////////////////////////////////
	AppKit.RadioController = function( i_View, i_Model ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Controller.apply( this, [i_View, i_Model] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_InputChangeSlot : {
				value 		: new AppKit.Slot()
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.HandleRefreshView = function( i_Args ) {
			this.m_View.Element().checked = this.m_Model.IsSelected(); 
		}; 
		
		this.__prototype__.HandleInputChange = function( i_Args ) {
			this.m_Model.SetIsSelected( this.m_View.Element().checked, false ); 
		}; 
		
		// Constructor
		{
			this.m_RefreshViewSlot.SetSelector( this.HandleRefreshView ); 
			this.m_RefreshViewSlot.SetContext( this ); 
			
			this.m_Model.RefreshViewSignal().Connect( this.m_RefreshViewSlot ); 
			
			this.m_InputChangeSlot.SetSelector( this.HandleInputChange ); 
			this.m_InputChangeSlot.SetContext( this ); 
			
			this.m_View.ChangeSignal().Connect( this.m_InputChangeSlot ); 
			
		}
		
	}; 
	
	AppKit.RadioController.prototype = new AppKit.Controller; 
	
}); 
