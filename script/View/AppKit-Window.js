define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View", "AppKit/View/AppKit-Button", "AppKit/View/AppKit-TitleBar"], function(){	
	///////////////////////////////////////////////////////////////////////////////
	// Window : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Window = function( i_Title, i_ZIndex, i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this );
		
		AppKit.View.apply( this
			, [(i_Size && i_Size instanceof AppKit.Size 
					? i_Size 
					: new AppKit.Size( new AppKit.Distance( 100, "%" )
				, new AppKit.Distance( 100, "%" ) ))
				, i_Position
				, i_Element
				, i_Tag
			] 
		); 
		
		// Data Members
		Object.defineProperties(this, {
			m_DestroySignal : {
				value		: new AppKit.Signal()
				, writable	: true
			}
			, m_TitleBar : {
				value		: new AppKit.TitleBar( "AppKit Window", [], [], this )
				, writable	: true
			}
			, m_ContentElement : {
				value		: document.createElement( "div" )
				, writable	: true
			}
			, m_OverlayElement : {
				value		: document.createElement( "div" )
				, writable	: true
			}
			, m_ZIndex : {
				value		: 0
				, writable	: true
			}
			, m_ParentWindow : {
				value		: null
				, writable	: true
			}
			, m_ChildWindows : {
				value		: []
				, writable	: true
			}
			, m_InFocus : {
				value		: true
				, writable	: true
			}
			, m_IsModal : {
				value		: false
				, writable 	: true
			}
			, m_OverlayVisible : {
				value		: false
				, writable	: true
			}
			, m_OverlayInterval : {
				value		: null
				, writable	: true
			}
			, m_OverlayOpacity : {
				value		: 0.0
				, writable	: true
			}
			
		});
		
		// Function Members & Inheritance
		this.__prototype__.Destroy = function() {
			for( var i = this.m_ChildWindows.length - 1; i > -1; --i ) {
				this.m_ChildWindows[i].Destroy(); 
			}
			
			if( this.m_ParentWindow ) { 
				if( this.m_IsModal ) {
					this.m_ParentWindow.HideOverlay(); 
				}
					
				for( var i = 0; i < this.m_ParentWindow.m_ChildWindows.length; ++i ) {
					if( this.m_ParentWindow.m_ChildWindows[i] === this ) {
						this.m_ParentWindow.m_ChildWindows.splice( i, 1 ); 
					}
					
				}
				
			}
			
			document.body.removeChild( this.m_Element ); 
	
			this.m_DestroySignal.Emit( [this] ); 
			
			delete this; 
			
		}; 
		
		this.__prototype__.DestroySignal = function() {
			return this.m_DestroySignal; 
		}; 
	
		this.__prototype__.SetParentWindow = function( i_Window ) {
			if( !i_Window || !(i_Window instanceof AppKit.Window) ) {
				throw new Error( "parameter i_Window for AppKit::Window::SetParentWindow() is of incorrect type; expecting AppKit.Window" ); 
			}
		
			this.m_ParentWindow = i_Window; 
						
		}; 
		
		this.__prototype__.ParentWindow = function() {
			return this.m_ParentWindow; 
		}; 
		
		this.__prototype__.ContentElement = function() {
			return this.m_ContentElement; 
		}; 
		
		this.__prototype__.TitleBar = function() {
			return this.m_TitleBar; 
		}; 
		
		this.__prototype__.Placement = function() {
			if( (this.m_ZIndex % 100000) == 0 ) {
				return 10000; 
			} else if( (this.m_ZIndex % 10000) == 0 ) {
				return 1000; 
			} else if( (this.m_ZIndex % 1000) == 0 ) {
				return 100; 
			} else if( (this.m_ZIndex % 100) == 0 ) {
				return 10; 
			}
			
			return 1; 
			
		}; 
		
		this.__prototype__.SetZIndex = function( i_ZIndex ) {
			this.SetInFocus( false  ); 
		
			this.m_ZIndex = Number( i_ZIndex ); 
			this.m_Element.style.zIndex = this.m_ZIndex; 
			
			for( var i = 0; i < this.m_ChildWindows.length; ++i ) {
				this.m_ChildWindows[i].SetZIndex( this.m_ZIndex + (this.Placement() * (i + 1)) ); 
			}
			
		}; 
		
		this.__prototype__.ZIndex = function() {
			return this.m_ZIndex; 
		}; 
		
		this.__prototype__.SetInFocus = function( i_InFocus ) {
			this.m_Element.className = this.m_Element.className.replace( /(?:^|\s)AppKit-ActiveWindow(?!\S)/ , "" ).replace( /^\s+|\s+$/g, "" ); 
			
			this.m_InFocus = i_InFocus ? true : false; 
			
			if( this.m_InFocus ) {
				this.m_Element.className += " AppKit-ActiveWindow"; 
			}
			
		}; 
		
		this.__prototype__.IsModal = function() {
			return this.m_IsModal; 
		}; 
		
		this.__prototype__.SetIsModal = function( i_IsModal ) {
			this.m_IsModal = i_IsModal; 
		}; 
		
		this.__prototype__.BringFront = function( i_InFocus ) {
			if( this.m_ParentWindow ) {
				var Placement = this.m_ParentWindow.Placement(); 
				
				for( var i = 0; i < this.m_ParentWindow.m_ChildWindows.length; ++i ) {
					this.m_ParentWindow.m_ChildWindows[i].SetInFocus( false ); 
					
					if( this.m_ParentWindow.m_ChildWindows[i] === this ) {
						this.m_ParentWindow.m_ChildWindows.splice( i, 1 ); 
						this.m_ParentWindow.m_ChildWindows.push( this ); 
					}
					
					this.m_ParentWindow.m_ChildWindows[i].SetZIndex( this.m_ParentWindow.ZIndex() + (Placement * (i + 1)) ); 
					
				}
				
				this.m_ParentWindow.BringFront( false ); 
				
			}
			
			this.SetInFocus( i_InFocus ? true : false ); 
			
		}; 
		
		this.__prototype__.InFront = function() {
			return this.m_InFront; 
		}; 
		
		this.__prototype__.AddChildWindow = function( i_Window, i_Modal ) {
			if( !i_Window || !(i_Window instanceof AppKit.Window) ) {
				throw new Error( "parameter i_Window for AppKit::Window::AddChildWindow() is of incorrect type; expecting AppKit.Window" ); 
			}
			
			this.m_ChildWindows.push( i_Window ); 
			document.body.appendChild( i_Window.Element() ); 
			
			i_Window.SetParentWindow( this ); 
			i_Window.BringFront( true ); 
			
			if( i_Modal ) {
				i_Window.SetIsModal( true ); 
				this.ShowOverlay(); 
			}

			return i_Window; 
			
		}; 
		
		this.__prototype__.RemoveChildWindow = function( i_Window ) {
			
		}; 
		
		this.__prototype__.Append = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.View) ) {
				throw new Error( "parameter i_View for AppKit::Window::Append() is of incorrect type; expecting AppKit.View" ); 
			}
			
			this.m_ContentElement.appendChild( i_View.Element() ); 
			
		}; 
		
		this.__prototype__.ShowOverlay = function() {
	/*
			if( !this.m_OverlayInterval )
				this.m_OverlayInterval = setInterval( function(){ this.ShowOverlay(); }.bind(this), 10 ); 
			
			if( !this.m_OverlayVisible ) {
				this.m_OverlayVisible = true; 
				this.m_OverlayElement.style.display = "block";
				
			}
			
			this.m_OverlayOpacity += 0.2;
			
			if( this.m_OverlayOpacity >= 0.7 ) {
				this.m_OverlayOpacity = 0.7;
				clearInterval( this.m_OverlayInterval ); 
			} 
			
			this.m_OverlayElement.style.opacity = this.m_OverlayOpacity; 
	*/
			
			this.m_OverlayElement.style.display = "block";
			this.m_OverlayElement.style.opacity = 0.6; 
			
			this.m_OverlayOpacity = 0.6; 
			this.m_OverlayVisible = true; 
			
		}; 
		
		this.__prototype__.HideOverlay = function() {
	/*
			if( !this.m_OverlayInterval )
				this.m_OverlayInterval = setInterval( function(){ this.HideOverlay(); }.bind(this), 10 ); 
				
			if( !this.m_OverlayVisible ) {
				this.m_OverlayVisible = true; 
				this.m_OverlayElement.style.display = "block";
			}
			
			this.m_OverlayOpacity -= 0.2;
			
			this.m_OverlayElement.style.opacity = this.m_OverlayOpacity; 
			
			if( this.m_OverlayOpacity <= 0.0 ) {
				this.m_OverlayOpacity = 0.0;
				
				
				
				clearInterval( this.m_OverlayInterval ); 
				
				console.log( "clear interval" ); 
				
			} 
	*/
			
			this.m_OverlayElement.style.display = "none";
			this.m_OverlayElement.style.opacity = 0.0; 
			
			this.m_OverlayOpacity = 0.0; 
			this.m_OverlayVisible = false; 
	
		}; 
		
		// Constructor
		{	
			if( i_Title ) 	this.m_TitleBar.SetTitle( i_Title ); 
			if( i_ZIndex ) 	this.SetZIndex( i_ZIndex ); 
			
			var CloseButton = new AppKit.Button( "Close" ); 
			CloseButton.SelectSignal().Connect( new AppKit.Slot( function(){ this.Destroy(); }, this) ); 
			
			this.m_TitleBar.SetLHSelectors( [CloseButton] ); 
			
			this.m_Element.onmousedown = function(){ this.BringFront( true ); }.bind(this); 
			
			this.m_ContentElement.className = "AppKit-View AppKit-Window-Content"; 
			this.m_OverlayElement.className = "AppKit-View AppKit-Window-Overlay"; 
			
			this.m_Element.className += " AppKit-Window"; 
			
			this.m_Element.appendChild( this.m_TitleBar.Element() ); 
			this.m_Element.appendChild( this.m_ContentElement ); 
			this.m_Element.appendChild( this.m_OverlayElement ); 
			
		}
		
	}; 
	
	AppKit.Window.prototype = new AppKit.View; 
	
}); 
