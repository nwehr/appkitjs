define(["AppKit/Core/AppKit-Base"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Distance
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Distance = function( i_Dist, i_Unit ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Dist : {
				value		: 0
				, writable	: true
			}
			, m_Unit : {
				value		: "px"
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetDist = function( i_Dist ) {
			if( typeof i_Dist == "number" ) this.m_Dist = i_Dist; 
		}; 
		
		this.__prototype__.Dist = function() {
			return this.m_Dist; 
		}; 
		
		this.__prototype__.SetUnit = function( i_Unit ) {
			if( typeof i_Unit == "string" ) this.m_Unit = i_Unit; 
		}; 
		
		this.__prototype__.Unit = function() {
			return this.m_Unit; 
		}; 
		
		// Constructor
		{
			if( i_Dist ) this.SetDist( i_Dist ); 
			if( i_Unit ) this.SetUnit( i_Unit ); 
		}
		
	}; 
		
	///////////////////////////////////////////////////////////////////////////////
	// Size
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Size = function( i_WidthDistance, i_HeightDistance ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_WidthDistance : {
				value		: null
				, writable	: true
			}
			, m_HeightDistance : {
				value		: null
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetWidthDistance = function( i_WidthDistance ) {
			if( !(i_WidthDistance == null) && !(i_WidthDistance instanceof AppKit.Distance) )
				throw new Error( "parameter i_WidthDistance for AppKit::Size::SetWidthDistance() is of incorrect type; expecting AppKit.Distance or null" ); 

			this.m_WidthDistance = i_WidthDistance; 
			
		}; 
		
		this.__prototype__.WidthDistance = function() {
			return this.m_WidthDistance; 
		}; 
		
		this.__prototype__.Width = function() {
			return (this.m_WidthDistance.Dist() + this.m_WidthDistance.Unit()); 
		}; 
		
		this.__prototype__.SetHeightDistance = function( i_HeightDistance ) {
			if( !(i_WidthDistance == null) && !(i_WidthDistance instanceof AppKit.Distance) )
				throw new Error( "parameter i_HeightDistance for AppKit::Size::SetHeightDistance() is of incorrect type; expecting AppKit.Distance or null" ); 

			this.m_HeightDistance = i_HeightDistance; 

		}; 
		
		this.__prototype__.HeightDistance = function() {
			return this.m_HeightDistance; 
		}; 
		
		this.__prototype__.Height = function() {
			return (this.m_HeightDistance.Dist() + this.m_HeightDistance.Unit()); 
		}; 
		
		// Constructor
		{
			if( i_WidthDistance ) 	this.SetWidthDistance	( i_WidthDistance ); 
			if( i_HeightDistance )	this.SetHeightDistance	( i_HeightDistance ); 
		}
		
	}; 
	
}); 
