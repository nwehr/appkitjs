define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Heading : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Heading = function( i_Text, i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, (i_Element ? i_Element : document.createElement( "h1" )), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {}); 
		
		// Function Members
		this.__prototype__.Text = function() {
			return this.m_Element.innerHTML; 
		}; 
		
		this.__prototype__.SetText = function( i_Text ) {
			this.m_Element.innerHTML = i_Text; 
		}; 
		
		// Constructor 
		{
			if( i_Text ) this.SetText( i_Text ); 
			
			this.AddClass( "AppKit-Heading" ); 
			
		}

	}; 
	
	AppKit.Heading.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Heading1 : Heading
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Heading1 = function( i_Text, i_Size, i_Position, i_Tag ) {
		AppKit.Heading.apply( this, [i_Text, i_Size, i_Position, document.createElement( "h1" ), i_Tag] ); 
	}; 
	
	AppKit.Heading1.prototype = new AppKit.Heading; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Heading2 : Heading
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Heading2 = function( i_Text, i_Size, i_Position, i_Tag ) {
		AppKit.Heading.apply( this, [i_Text, i_Size, i_Position, document.createElement( "h2" ), i_Tag] ); 
	}; 
	
	AppKit.Heading2.prototype = new AppKit.Heading; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Heading3 : Heading
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Heading3 = function( i_Text, i_Size, i_Position, i_Tag ) {
		AppKit.Heading.apply( this, [i_Text, i_Size, i_Position, document.createElement( "h3" ), i_Tag] ); 
	}; 
	
	AppKit.Heading3.prototype = new AppKit.Heading; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Heading4 : Heading
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Heading4 = function( i_Text, i_Size, i_Position, i_Tag ) {
		AppKit.Heading.apply( this, [i_Text, i_Size, i_Position, document.createElement( "h4" ), i_Tag] ); 
	}; 
	
	AppKit.Heading4.prototype = new AppKit.Heading; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Paragraph : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Paragraph = function( i_Text, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "p" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {}); 
		
		// Function Members
		this.__prototype__.Text = function() {
			return this.m_Element.innerHTML; 
		}; 
		
		this.__prototype__.SetText = function( i_Text ) {
			this.m_Element.innerHTML = i_Text; 
		}; 
		
		// Constructor 
		{
			if( i_Text ) this.SetText( i_Text ); 
			
			this.AddClass( "AppKit-Paragraph" ); 
			
		}
		
	}; 
	
	AppKit.Paragraph.prototype = new AppKit.View; 
	
}); 
