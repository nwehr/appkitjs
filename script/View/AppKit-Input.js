define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Label : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Label = function( i_For, i_Text, i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "label" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {}); 
		
		// Function Members
		this.__prototype__.For = function() {
			return this.m_Element.getAttribute( "for" ); 
		}; 
		
		this.__prototype__.SetFor = function( i_For ) {
			this.m_Element.setAttribute( "for", i_For ); 
		}; 
		
		this.__prototype__.Text = function() {
			return this.m_Element.innerHTML; 
		}; 
		
		this.__prototype__.SetText = function( i_Text ) {
			this.m_Element.innerHTML = i_Text; 
		}; 
		
		// Constructor
		{
			if( i_For )		this.SetFor	( i_For ); 
			if( i_Text ) 	this.SetText( i_Text ); 
			
			this.m_Element.className += " AppKit-Label"; 
			
		}
		
	}; 
	
	AppKit.Label.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Textarea : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Textarea = function( i_Text, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "textarea" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_ChangeSignal : {
				value 		: new AppKit.Signal()
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Text = function() {
			return this.m_Element.value; 
		}; 
		
		this.__prototype__.SetText = function( i_Text, i_DoEmitSignal ) {
			this.m_Element.value = i_Text; 
			
			if( i_DoEmitSignal )
				this.m_ChangeSignal.Emit( [this] ); 
			
		}; 
		
		this.__prototype__.ChangeSignal = function() {
			return this.m_ChangeSignal; 
		}; 
		
		// Constructor
		{
			if( i_Text ) this.SetText( i_Text, false ); 
			
			this.m_Element.className += " AppKit-Input"; 
			this.m_Element.onchange = function() { this.m_ChangeSignal.Emit( [this] ); }.bind(this); 
			
		}
		
	}; 
	
	AppKit.Textarea.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Input : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Input = function( i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "input" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_ChangeSignal : {
				value 		: new AppKit.Signal()
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.ID = function() {
			return this.m_Element.id; 
		}; 
		
		this.__prototype__.SetID = function( i_ID ) {
			this.m_Element.id = i_ID; 
		}; 
		
		this.__prototype__.Name = function() {
			return this.m_Element.name; 
		}; 
		
		this.__prototype__.SetName = function( i_Name ) {
			this.m_Element.name = i_Name; 
		}; 
		
		this.__prototype__.Value = function() {
			return this.m_Element.value; 
		}; 
		
		this.__prototype__.SetValue = function( i_Value, i_DoEmitSignal ) {
			this.m_Element.value = i_Value; 
			
			if( i_DoEmitSignal )
				this.m_ChangeSignal.Emit( [this] ); 
			
		}; 
		
		this.__prototype__.ChangeSignal = function() {
			return this.m_ChangeSignal; 
		}; 
		
		// Constructor
		{
			if( i_ID )		this.SetID		( i_ID ); 
			if( i_Name )	this.SetName	( i_Name ); 
			if( i_Value ) 	this.SetValue	( i_Value, false ); 
			
			this.m_Element.className += " AppKit-Input"; 
			this.m_Element.onchange = function() { this.m_ChangeSignal.Emit( [this] ); }.bind(this); 
			
		}
		
	}; 
	
	AppKit.Input.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Text : Input
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Text = function( i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		AppKit.Input.apply( this, [i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag] ); 
		
		// Constructor
		{
			this.m_Element.type = "text"; 
		}
		
	}; 
	
	AppKit.Text.prototype = new AppKit.Input; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Radio : Input
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Radio = function( i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		AppKit.Input.apply( this, [i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag] ); 
		
		// Constructor
		{
			this.m_Element.type = "radio"; 
		}
		
	}; 
	
	AppKit.Radio.prototype = new AppKit.Input; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Checkbox : Input
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Checkbox = function( i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		AppKit.Input.apply( this, [i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag] ); 
		
		// Constructor
		{
			this.m_Element.type = "checkbox"; 
		}
		
	}; 
	
	AppKit.Checkbox.prototype = new AppKit.Input; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Password : Input
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Password = function( i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		AppKit.Input.apply( this, [i_ID, i_Name, i_Value, i_Size, i_Position, i_Tag] ); 
		
		// Constructor
		{
			this.m_Element.type = "password"; 
		}
		
	}; 
	
	AppKit.Password.prototype = new AppKit.Input; 
	
}); 
