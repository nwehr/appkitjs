define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View", "AppKit/View/AppKit-Button"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// TitleBar : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.TitleBar = function( i_Title, i_LHSelectors, i_RHSelectors, i_Window, i_Size, i_Position, i_Element ) {
		this.__prototype__ = Object.getPrototypeOf(this); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "div" ), null] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Window : {
				value		: null
				, writable	: true
			}
			, m_TitleElement : {
				value		: document.createElement( "span" )
				, writable	: true
			}
			, m_LHSelectors : {
				value		: []
				, writable	: true
			}
			, m_RHSelectors : {
				value		: []
				, writable	: true
			}
			, m_DragActive : {
				value		: false
				, writable	: true
			}
			, m_DragOffsetX : {
				value		: 0
				, writable	: true
			}
			, m_DragOffsetY : {
				value		: 0
				, writable	: true
			}
			
		});
		
		// Function Members 
		this.__prototype__.Title = function() {
			return this.m_TitleElement.innerText; 
		}; 
		
		this.__prototype__.SetTitle = function( i_Title ) {
			return this.m_TitleElement.innerText = i_Title; 
		}; 
		
		this.__prototype__.Hide = function() {
			this.m_Element.style.display = "none"; 
		}; 
		
		this.__prototype__.Show = function() {
			this.m_Element.style.display = "block"; 
		}; 
		
		this.__prototype__.SetLHSelectors = function( i_LHSelectors ) {
			if( !i_LHSelectors || !(i_LHSelectors instanceof Array) ) {
				throw new Error( "parameter i_LHSelectors for AppKit::TitleBar::SetLHSelectors() is of incorrect type; expecting Array" ); 
			}
			
			// Removing old selectors
			for( var i = 0; i < this.m_LHSelectors.length; ++i ) {
				this.m_Element.removeChild( this.m_LHSelectors[i].Element() ); 
			}
			
			this.m_LHSelectors = []; 
			
			for( var i = 0; i < i_LHSelectors.length; ++i ) {
				if( !(i_LHSelectors[i] instanceof AppKit.Button) && !(i_LHSelectors[i] instanceof AppKit.DropdownButton) ) {
					throw new Error( "parameter member for AppKit::TitleBar::SetLHSelectors() is of incorrect type; expecting AppKit.Button or AppKit.DropdownButton" ); 
				}
				
				this.m_LHSelectors.push( i_LHSelectors[i] ); 
				
				this.m_LHSelectors[i].Element().style.cssFloat = "left"; 
				this.m_LHSelectors[i].Element().className += " AppKit-Selector"; 
				
				this.m_Element.appendChild( this.m_LHSelectors[i].Element() ); 
				
			}
			
		}; 
		
		this.__prototype__.SetRHSelectors = function( i_RHSelectors ) {
			if( !i_RHSelectors || !(i_RHSelectors instanceof Array) ) {
				throw new Error( "parameter i_RHSelectors for AppKit::TitleBar::SetRHSelectors() is of incorrect type; expecting Array" ); 
			}
			
			// Removing old selectors
			for( var i = 0; i < this.m_RHSelectors.length; ++i ) {
				this.m_Element.removeChild( this.m_RHSelectors[i].Element() ); 
			}
			
			this.m_RHSelectors = []; 
		
			for( var i = 0; i < i_RHSelectors.length; ++i ) {
				if( !(i_RHSelectors[i] instanceof AppKit.Button) && !(i_RHSelectors[i] instanceof AppKit.DropdownButton) ) {
					throw new Error( "parameter member for AppKit::TitleBar::SetRHSelectors() is of incorrect type; expecting AppKit.Button or AppKit.DropdownButton" ); 
				}
				
				this.m_RHSelectors.push( i_RHSelectors[i] ); 
				
				this.m_RHSelectors[i].Element().style.cssFloat = "right"; 
				this.m_RHSelectors[i].Element().className += " AppKit-Selector"; 
				
				this.m_Element.appendChild( this.m_RHSelectors[i].Element() ); 
				
			}
			
		}; 
		
		this.__prototype__.Drag = function( e ) {
			var evt = e || window.event; 
			
			if( this.m_DragActive ) {
				var X = 0; 
				var Y = 0; 
				
				if( typeof pageXOffset == 'number' ) {
					X = (evt.clientX - this.m_DragOffsetX); 
					Y = (evt.clientY - this.m_DragOffsetY); 
				} else {
					X = (evt.layerX - this.m_DragOffsetX); 
					Y = (evt.layerY - this.m_DragOffsetY); 
				}
				
				this.m_Window.SetPosition( new AppKit.AbsolutePosition( new AppKit.Coordinate( X, "px" ), new AppKit.Coordinate( Y, "px" ) ) ); 
				
			}
			
		}; 
		
		this.__prototype__.Grab = function(e) {
			this.m_DragActive 	= true; 
			
			var evt = e || window.event; 
			
			if( typeof pageXOffset == 'number' ) {
				this.m_DragOffsetX 	= evt.clientX - this.m_Window.Position().XCoordinate().Coord(); 
				this.m_DragOffsetY 	= evt.clientY - this.m_Window.Position().YCoordinate().Coord(); 
			} else {
				this.m_DragOffsetX 	= evt.layerX - this.m_Window.Position().XCoordinate().Coord(); 
				this.m_DragOffsetY 	= evt.layerY - this.m_Window.Position().YCoordinate().Coord(); 
			}
			
			document.body.addEventListener( "mousemove", this.Drag.bind(this), true ); 
			document.body.addEventListener( "mouseup", this.UnGrab.bind(this), true ); 
			
		}; 
		
		this.__prototype__.UnGrab = function() {
			this.m_DragActive = false; 
			
			document.body.removeEventListener( "mousemove", this.Drag.bind(this) ); 
			document.body.removeEventListener( "mouseup", this.UnGrab.bind(this) ); 
			
		}; 
		
		// Constructor
		{
			this.m_TitleElement.innerText = (i_Title ? i_Title : "AppKit Window"); 
			
			this.m_Element.className += " AppKit-TitleBar"; 
			this.m_Element.onmousedown 	= this.Grab.bind(this); 
			this.m_Element.appendChild( this.m_TitleElement ); 
			
			if( i_LHSelectors ) this.SetLHSelectors( i_LHSelectors ); 
			if( i_RHSelectors ) this.SetRHSelectors( i_RHSelectors ); 
			
			if( i_Window ) this.m_Window = i_Window; 
			
		}
		
	}; 
	
	AppKit.TitleBar.prototype = new AppKit.View; 
	
}); 
