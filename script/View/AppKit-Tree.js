define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-Scroll"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Node : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Node = function( i_Text, i_Tree, i_DataNode, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [null, null, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Parent : {
				value 		: null
				, writable 	: true
			}
			, m_Tree		: {
				value		: null
				, writable	: true
			}
			, m_THElement : {
				value		: document.createElement( "div" )
				, writable	: true
			}
			, m_LHElement : {
				value		: document.createElement( "div" ) 
				, writable	: true
			}
			, m_RHElement : {
				value 		: document.createElement( "span" )
				, writable 	: true
			}
			, m_IsSelected : {
				value		: false
				, writable	: true
			}
			, m_Tabs : {
				value 		: -1
				, writable 	: true
			}
			, m_Margin : {
				value		: 1.2
				, writable	: false
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Empty = function() {}
		
		this.__prototype__.SetParent = function( i_Node ) {
			if( !i_Node || !(i_Node instanceof AppKit.Branch) ) {
				throw new Error( "parameter i_Node for AppKit::Node::SetParent() is of incorrect type; expecting AppKit.Branch" ); 
			}
			
			this.m_Parent = i_Node; 
			
		}; 
		
		this.__prototype__.Parent = function() {
			return this.m_Parent; 
		}; 
		
		this.__prototype__.SetTree = function( i_Tree ) {
			if( !i_Tree || !(i_Tree instanceof AppKit.Tree) ) {
				throw new Error( "parameter i_Tree for AppKit::Node::SetTree() is of incorrect type; expecting AppKit.Tree" ); 
			}
			
			this.m_Tree = i_Tree;
			
		};
		
		this.__prototype__.Tree = function() {
			return this.m_Tree;
		};
		
		this.__prototype__.SetTabs = function( i_Tabs ) {
			this.m_Tabs = i_Tabs; 
			
			this.m_LHElement.style.marginLeft = ((this.m_Tabs * this.m_Margin) + 0.4) + "em";
			
		}; 
		
		this.__prototype__.SetText = function( i_Text ) {
			this.m_RHElement.innerHTML = i_Text; 
		}; 
		
		this.__prototype__.Text = function() {
			return this.m_RHElement.innerHTML; 
		}; 
		
		this.__prototype__.SetSize 		= function( i_Size ) {}; 
		this.__prototype__.SetPosition 	= function( i_Position ) {}; 
		
		this.__prototype__.Select = function() {
			if( !this.m_IsSelected ) {
				this.AddClass( "AppKit-Selected" ); 
				this.m_IsSelected = true; 
				this.m_Tree.SelectNode( this, true );
				
			}
			
		};
		
		this.__prototype__.UnSelect = function() {
			this.RemoveClass( "AppKit-Selected" ); 
			this.m_IsSelected = false; 
			this.m_Tree.UnSelectNode( this );
		};
		
		this.__prototype__.DoubleSelect = function() {
			if( !this.m_Selected )
				this.Select();
			
			this.m_Tree.DoubleSelectNode( this ); 
		}; 
		
		this.__prototype__.HandleClick = function(e) {
			this.Select();
		};
		
		this.__prototype__.HandleDoubleClick = function(e) {
			this.DoubleSelect(); 
		}; 
		
		this.__prototype__.Path = function( i_Path ) {
			var Path = i_Path ? i_Path : [];
			
			if( this.m_Parent ) {
				for( var i = 0; i < this.m_Parent.m_ChildNodes.length; ++i ) {
					if( this.m_Parent.m_ChildNodes[i] === this ) {
						Path.unshift( i );
						Path = this.m_Parent.Path( Path ); 
					}
					
				}
				
			}
			
			return Path;
			
		}; 
		
		// Constructor 
		{
			if( i_Text )		this.SetText( i_Text ); 
			if( i_DataNode ) 	this.SetDataNode( i_DataNode ); 
			if( i_Tree )		this.SetTree ( i_Tree );
			
			this.AddClass( "AppKit-Node" ); 
			
			this.m_Element.appendChild( this.m_THElement );
			this.m_THElement.appendChild( this.m_LHElement );
			this.m_THElement.appendChild( this.m_RHElement );
			
			this.m_RHElement.onclick 	= this.HandleClick.bind(this);
			this.m_RHElement.ondblclick = this.HandleDoubleClick.bind(this);  
			
		}
		
	}; 
	
	AppKit.Node.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Branch : Node
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Branch = function( i_Text, i_Tree, i_DataNode, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Node.apply( this, [i_Text, i_Tree, i_DataNode, i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_ChildNodes : {
				value 		: []
				, writable 	: true
			}
			, m_BHElement : {
				value 		: document.createElement( "div" )
				, writable 	: true
			}
			, m_IsCollapsed : {
				value		: true
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.ChildNodes = function() {
			return this.m_Children; 
		}; 
		
		this.__prototype__.Append = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.Node) ) {
				throw new Error( "parameter i_View for AppKit::Branch::AddChildNode() is of incorrect type; expecting AppKit.Node" ); 
			}
			
			i_View.SetTabs( this.m_Tabs + 1 ); 
			i_View.SetTree( this.m_Tree );
			i_View.SetParent( this ); 
			
			this.m_ChildNodes.push( i_View ); 
			this.m_BHElement.appendChild( i_View.Element() ); 
			
			return this.m_ChildNodes[this.m_ChildNodes.length - 1]; 
			
		}; 
		
		this.__prototype__.Empty = function() {
			this.m_ChildNodes = []; 
			this.m_BHElement.innerHTML = ""; 
		}; 
		
		this.__prototype__.Expand = function() {
			this.m_BHElement.style.display = "block"; 
			this.m_LHElement.style.marginLeft = ((this.m_Tabs * this.m_Margin) + 0.3) + "em"; 
			
			this.AddClass ( "AppKit-Expanded" );
			
			this.m_IsCollapsed = false; 
			
		}; 
		
		this.__prototype__.Collapse = function() {
			this.m_BHElement.style.display = "none"; 
			this.m_LHElement.style.marginLeft = ((this.m_Tabs * this.m_Margin) + 0.4) + "em"; 
			// this.m_LHElement.style.marginRight = "0.4em"; 
			
			this.RemoveClass ( "AppKit-Expanded" );
			
			this.m_IsCollapsed = true; 
			
		}; 
		
		this.__prototype__.HandleVisibility = function( e ) {
			if( this.m_IsCollapsed ) {
				this.Expand(); 
				this.m_IsCollapsed = false; 
			} else {
				this.Collapse(); 
				this.m_IsCollapsed = true; 
			}
		}; 
		
		// Constructor 
		{
			this.AddClass( "AppKit-Branch" ); 
			
			this.m_LHElement.onclick = this.HandleVisibility.bind( this ); 
					
			this.m_Element.appendChild( this.m_BHElement );
			
			this.Collapse(); 
			
		}
		
	}; 
	
	AppKit.Branch.prototype = new AppKit.Node; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Leaf : Node
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Leaf = function( i_Text, i_Tree, i_DataNode, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Node.apply( this, [i_Text, i_Tree, i_DataNode, i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			
		}); 
		
		// Function Members
		
		// Constructor 
		{
			this.AddClass( "AppKit-Leaf" ); 
		}
		
	}; 
	
	AppKit.Leaf.prototype = new AppKit.Node; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Tree : Scroll
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Tree = function( i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Scroll.apply( this, [i_Size, i_Position, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Root : {
				value 		: new AppKit.Branch( "Root", this )
				, writable 	: true
			}
			, m_SelectedNodes : {
				value		: []
				, writable	: true
			}
			, m_SelectSignal : {
				value		: new AppKit.Signal()
				, writable	: true
			}
			, m_DoubleSelectSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Root = function() {
			return this.m_Root; 
		}; 
		
		this.__prototype__.SelectSignal = function() {
			return this.m_SelectSignal; 
		}; 
		
		this.__prototype__.DoubleSelectSignal = function() {
			return this.m_DoubleSelectSignal; 
		}; 
		
		this.__prototype__.SelectNode = function( i_Node, i_UnSelectOthers ) {
			if( !i_Node || !(i_Node instanceof AppKit.Node) ) {
				throw new Error( "parameter i_Node for AppKit::Branch::SelectNode() is of incorrect type; expecting AppKit.Node" ); 
			}
			
			if( i_UnSelectOthers ) {
				for( var i = this.m_SelectedNodes.length -1; i > -1; --i ) {
					this.m_SelectedNodes[i].UnSelect();
					this.m_SelectedNodes.pop();
				}
				
			}
			
			this.m_SelectedNodes.push( i_Node );
			
			this.m_SelectSignal.Emit( [this, i_Node] ); 
			
		};
		
		this.__prototype__.DoubleSelectNode = function( i_Node ) {
			if( !i_Node || !(i_Node instanceof AppKit.Node) ) {
				throw new Error( "parameter i_Node for AppKit::Branch::DoubleSelectNode() is of incorrect type; expecting AppKit.Node" ); 
			}
			
			this.m_DoubleSelectSignal.Emit( [this, i_Node] ); 
			
		}; 
		
		this.__prototype__.UnSelectNode = function( i_Node ) {
			for( var i = 0; i < this.m_SelectedNodes.length; ++i ) {
				if( this.m_SelectedNodes[i] === i_Node ) {
					this.m_SelectedNodes.splice( i, 1 );
				}
				
			}
			
		};
		
		this.__prototype__.GetNode = function( i_Path, i_Root ) {
			var NewRoot = i_Root ? i_Root.m_ChildNodes[i_Path[0]] : this.m_Root.m_ChildNodes[i_Path[0]]; 
			
			if( i_Path.length > 1 ) {
				i_Path.shift(); 
				NewRoot = this.GetNode( i_Path, NewRoot ); 
			}
				
			return NewRoot; 
			
		}; 
		
		// Constructor 
		{
			this.AddClass( "AppKit-Tree" ); 
			
			this.Append( this.m_Root );
			
			this.m_Root.Element().childNodes[0].style.display = "none"; 
			this.m_Root.Expand();
			
		}
		
	}; 
	
	AppKit.Tree.prototype = new AppKit.Scroll; 
	
	
}); 
