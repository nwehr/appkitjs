define(["AppKit/Core/AppKit-Base", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// ToolTip : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.ToolTip = function( i_Text, i_Size, i_Position, i_Element ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, i_Element] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_TextElement : {
				value 		: document.createElement( "span" )
				, writable	: true
			}
			, m_ArrowElement : {
				value		: document.createElement( "div" )
				, writable	: true
			}
			
		}); 
		
		
		// Function Members & Inheritance
		this.__prototype__.SetText = function( i_Text ) {
			
		}; 
		
		this.__prototype__.Show = function() {
			
		}; 
		
		this.__prototype__.Hide = function() {
			
		}; 
		
		// Constructor 
		{
			this.m_TextElement.innerHTML = i_Text ? i_Text : "Tool Tip"; 
			
			this.m_ArrowElement.className = "AppKit-ToolTip-Arrow"; 
			
			this.m_Element.className += " AppKit-ToolTip"; 
			
			this.m_Element.appendChild( this.m_TextElement ); 
			this.m_Element.appendChild( this.m_ArrowElement ); 
			
		}
		
	}; 
	
	AppKit.ToolTip.prototype = new AppKit.View; 
	
}); 
