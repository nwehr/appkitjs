define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// SourceListGroup : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.SourceListGroup = function() {
		this.__prototyep__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_SourceListItems : {
				value 		: []
				, writable 	: true
			}
		}); 
		
		// Function Members
		this.__prototype__.Items = function() {
			return this.m_Items; 
		}; 
		
		// Constructor
		{}
	}; 
	
	AppKit.SourceListGroup.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// SourceList : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.SourceList = function( i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			, m_SourceListGroups : {
				value		: []
				, writable	: true
			}
		}); 
		
		// Function Members
		this.__prototype__.HeaderElement = function() {
			return this.m_HeaderElement; 
		}; 
		
		this.__prototype__.FooterElement = function() {
			return this.m_FooterElement; 
		}; 
		
		this.__prototype__.SelectedCells = function() {
			return this.m_SelectedCells; 
		}; 
		
		this.__prototype__.CreateHeaderCell = function( i_Title ) {
			return new AppKit.HeaderCell( i_Title ); 
		}; 
		
		this.__prototype__.CreateCell = function( i_Value ) {
			return new AppKit.Cell( i_Value ); 
		}; 
		
		this.__prototype__.HideHeader = function() {
			this.m_HeaderElement.style.display = "none"; 
		}; 
		
		this.__prototype__.ShowHeader = function() {
			this.m_HeaderElement.style.display = "normal"; 
		}; 
		
		this.__prototype__.SelectSignal = function() {
			return this.m_SelectSignal; 
		}
		
		this.__prototype__.SelectCell = function( i_Cell, i_UnSelectOthers ) {
			if( !i_Cell || !(i_Cell instanceof AppKit.Cell) ) {
				throw new Error( "parameter i_Cell for AppKit::Table::SelectCell() is of incorrect type; expecting AppKit.Cell" ); 
			}
			
			if( i_UnSelectOthers ) {
				for( var i = 0; i < this.m_SelectedCells.length ; ++i ) {
					AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Table::SelectCell() - debug - unselect cell and row " 
						+ this.m_SelectedCells[i].RowIndex() 
						+ " and column " 
						+ this.m_SelectedCells[i].ColumnIndex() 
					); 
						
					this.m_SelectedCells[i].UnSelect(); 
					
				}
				
				this.m_SelectedCells = []; 
				
			}
			
			this.m_SelectedCells.push( i_Cell ); 
			this.m_SelectedCells[this.m_SelectedCells.length - 1].Select(); 
			
			AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Table::SelectCell() - debug - select cell at row " 
				+ this.m_SelectedCells[this.m_SelectedCells.length - 1].RowIndex() 
				+ " and column " 
				+ this.m_SelectedCells[this.m_SelectedCells.length - 1].ColumnIndex() 
			); 
			
			this.m_SelectSignal.Emit( [this.m_SelectedCells[this.m_SelectedCells.length - 1]] ); 
			
		}; 
		
		this.__prototype__.AddColumn = function( i_Title ) {
			this.m_Columns.push( this.CreateHeaderCell( i_Title ) ); 
			
			this.m_HeaderElement.appendChild( this.m_Columns[this.m_Columns.length - 1].Element() ); 
			
			for( var i = 0; i < this.m_Rows.length; ++i ) {
				this.m_Rows[i].push( this.CreateCell( "" ) ); 
				this.m_BodyElement.childNodes[i].appendChild( this.m_Rows[i][this.m_Rows[i].length -1].Element() ); 
			}
			
		}; 
		
		this.__prototype__.AddRow = function() {
			this.m_Rows.push( [] ); 
			
			var Row = document.createElement( "tr" ); 
			
			for( var i = 0; i < this.m_Columns.length; ++i ) {
				this.m_Rows[this.m_Rows.length - 1].push( this.CreateCell() ); 
				Row.appendChild( this.m_Rows[this.m_Rows.length - 1][this.m_Rows[this.m_Rows.length - 1].length -1].Element() ); 
			}
			
			this.m_BodyElement.appendChild( Row ); 
			
		}
		
		this.__prototype__.ReloadValue = function( i_Cell, i_DataSourceRowIndex, i_DataSourceColumnIndex ) {
			i_Cell.SetValue( this.m_DataSource.GetValue( i_DataSourceRowIndex, i_DataSourceColumnIndex ) ); 
		};
		
		this.__prototype__.ReloadCell = function( i_RowIndex, i_ColumnIndex, i_DataSourceRowIndex, i_DataSourceColumnIndex ) {
			if( !(i_RowIndex < this.m_Rows.length) ) {
				for( var i = this.m_Rows.length; i < (i_RowIndex + 1); ++i ) {
					this.AddRow(); 
				}
				
			}
			
			if( !(i_ColumnIndex < this.m_Columns.length) ) {
				for( var i = this.m_Columns.length; i < (i_ColumnIndex + 1); ++i ) {
					this.AddColumn( this.m_Columns.length + 1 ); 
				}
				
			}
			
			this.m_Rows[i_RowIndex][i_ColumnIndex].SetRowIndex( i_RowIndex ); 
			this.m_Rows[i_RowIndex][i_ColumnIndex].SetColumnIndex( i_ColumnIndex ); 
			
			this.m_Rows[i_RowIndex][i_ColumnIndex].SetDataSourceRowIndex( i_DataSourceRowIndex ); 
			this.m_Rows[i_RowIndex][i_ColumnIndex].SetDataSourceColumnIndex( i_DataSourceColumnIndex ); 
			
			this.m_Rows[i_RowIndex][i_ColumnIndex].Element().onclick = function(){ this.SelectCell( this.m_Rows[i_RowIndex][i_ColumnIndex], true ); }.bind(this); 
			
			this.ReloadValue( this.m_Rows[i_RowIndex][i_ColumnIndex], i_DataSourceRowIndex, i_DataSourceColumnIndex ); 
			
		}; 
		
		this.__prototype__.DataSource = function() {
			return this.m_DataSource; 
		}
		
		this.__prototype__.SetDataSource = function( i_DataSource ) {
			if( !i_DataSource || !(i_DataSource instanceof AppKit.DataSource) ) {
				throw new Error( "parameter i_DataSource for AppKit::Table::SetDataSource() is of incorrect type; expecting AppKit.DataSource" ); 
			}
			
			this.m_DataSource = i_DataSource; 
			
		}; 
		
		this.__prototype__.ReloadColumns = function( i_Columns ) {
			if( !i_Columns || !(i_Columns instanceof Array) ) {
				throw new Error( "parameter i_Columns for AppKit::Table::ReloadColumns() is of incorrect type; expecting Array" ); 
			}
			
			for( var i = 0; i < this.m_DataSource.NumRows(); ++i ) {
				for( var n = 0; n < i_Columns.length; ++n ) {
					this.ReloadCell( i, n, i, i_Columns[n] ); 
				}
				
			}
			
		}; 
			
		// Constructor
		{
			this.m_Element.className = "AppKit-View AppKit-Table"; 
			
			this.m_TableElement.appendChild( this.m_HeaderElement );
			this.m_TableElement.appendChild( this.m_BodyElement ); 
			
			this.m_Element.appendChild( this.m_TableElement ); 
			this.m_Element.appendChild( this.m_FooterElement ); 
			
		}
		
	}; 
	
	AppKit.Table.prototype = new AppKit.View;
	
}); 
