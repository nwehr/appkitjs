define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Cell : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Cell = function( i_Table, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "td" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Table : {
				value 		: null
				, writable 	: true
			}
			, m_IsSelected : {
				value 		: false
				, writable 	: true
			}
			, m_Value : {
				value		: null
				, writable	: true
			}
			
		}); 
		
		// Function Members & Inheritance	
		this.__prototype__.SetTable = function( i_Table ) {
			if( !i_Table || !(i_Table instanceof AppKit.Table) ) {
				throw new Error( "parameter i_Table for AppKit::Cell::SetTable() is of incorrect type; expecting AppKit.Table" ); 
			}
			
			this.m_Table = i_Table; 
			
		}; 
		
		this.__prototype__.Table = function() {
			return this.m_Table; 
		};
			
		this.__prototype__.SetValue = function( i_Value ) {
			this.m_Value = i_Value; 
			this.m_Element.innerHTML = i_Value; 
		};
		
		this.__prototype__.Value = function() {
			return this.m_Value; 
		};
		
		this.__prototype__.IsSelected = function() {
			return this.m_IsSelected; 
		};
		
		this.__prototype__.Select = function( i_UnSelectOthers ) {
			this.AddClass( "AppKit-Selected" ); 
			
			this.m_IsSelected = true; 
			
			this.m_Table.SelectCell( this, i_UnSelectOthers ); 
			
		};
		
		this.__prototype__.DoubleSelect = function( i_UnSelectOthers ) {
			if( !this.m_Selected )
				this.Select( i_UnSelectOthers ); 
				
			this.m_Table.DoubleSelectCell( this );
			
		}; 
		
		this.__prototype__.UnSelect = function() {
			this.RemoveClass( "AppKit-Selected" ); 
			
			this.m_IsSelected = false; 
			
			this.m_Table.UnSelectCell( this ); 
			
		};
		
		this.__prototype__.HandleClick = function(e) {
			if( !this.m_Selected )
				this.Select( (e.ctrlKey == true ? false : true) ); 
				
		}; 
		
		this.__prototype__.HandleDoubleClick = function(e) {
			this.DoubleSelect(); 
		}; 
		
		this.__prototype__.RowIndex = function() {
			for( var i = 0; i < this.m_Element.parentNode.parentNode.childNodes.length; ++i ) {
				if( this.m_Element.parentNode.parentNode.childNodes[i] === this.m_Element.parentNode ) {
					return i; 
				}
				
			}
			
			return null; 
			
		}; 
		
		this.__prototype__.ColumnIndex = function() {
			for( var i = 0; i < this.m_Element.parentNode.childNodes.length; ++i ) {
				if( this.m_Element.parentNode.childNodes[i] === this.m_Element ) {
					return i; 
				}
				
			}
			
			return null; 
			
		}; 
		
		// Constructor
		{
			if( i_Table ) this.SetTable( i_Table ); 
			if( i_Value ) this.SetValue( i_Value );
			
			this.AddClass( "AppKit-Cell" ); 
			
			this.m_Element.onclick 		= this.HandleClick.bind( this ); 
			this.m_Element.ondblclick 	= this.HandleDoubleClick.bind( this ); 
			
		}
		
	}; 
	
	AppKit.Cell.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// MutableCell : Cell
	///////////////////////////////////////////////////////////////////////////////
	AppKit.MutableCell = function( i_Table, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Cell.apply( this, [i_Table, i_Value, i_Size, i_Position, i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Modified : {
				value 		: false
				, writable 	: true
			}
		}); 
		
		// Function Members
		this.__prototype__.HandleKeyPress = function( e ) {
			if( !this.m_Modified ) 
				this.m_Modified = true;

		}; 

		this.__prototype__.HandleBlur = function( e ) {
			if( this.m_Modified ) {
				this.SetValue( e.srcElement.innerHTML );
				this.m_Table.ChangeCell( this ); 
				this.m_Modified = false; 
			}

		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-MutableCell" ); 
			this.m_Element.setAttribute( "contenteditable", "true" ); 
			
			this.m_Element.onkeypress 	= this.HandleKeyPress.bind(this); 
			this.m_Element.onblur 		= this.HandleBlur.bind(this); 
			
		}
		
	}; 
	
	AppKit.MutableCell.prototype = new AppKit.Cell; 

	///////////////////////////////////////////////////////////////////////////////
	// HeaderCell : Cell
	///////////////////////////////////////////////////////////////////////////////
	AppKit.HeaderCell = function( i_Table, i_Value, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Cell.apply( this, [i_Table, i_Value, i_Size, i_Position, i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {}); 
		
		// Function Members
		this.__prototype__.Select 	= function() {}; 
		this.__prototype__.UnSelect = function() {}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-HeaderCell" ); 
		}
		
	}; 
	
	AppKit.HeaderCell.prototype = new AppKit.Cell; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Table : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Table = function( i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Rows : {
				value		: []
				, writable	: true
			}
			, m_Columns : {
				value 		: []
				, writable	: true
			}
			, m_TableElement : {
				value 		: document.createElement( "table" )
				, writable	: true
			}
			, m_HeaderElement : {
				value 		: document.createElement( "thead" )
				, writable	: true
			}
			, m_FooterElement : {
				value		: document.createElement( "div" )
				, writable	: true
			}
			, m_BodyElement : {
				value 		: document.createElement( "tbody" )
				, writable	: true
			}
			, m_IsMultipleSelect : {
				value 		: true
				, writable 	: true
			}
			, m_DoEmitScrollBottom : {
				value		: true
				, writable	: true
			}
			, m_SelectSignal : {
				value		: new AppKit.Signal()
				, writable	: true
			}
			, m_DoubleSelectSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			, m_ChangeSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			, m_ScrollBottomSignal : {
				value		: new AppKit.Signal()
				, writable	: true
			}
			, m_SelectedCells : {
				value 		: []
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.CreateCell = function( i_Value ) {
			return new AppKit.Cell( this, i_Value ); 
		}; 
		
		this.__prototype__.CreateHeaderCell = function( i_Value ) {
			return new AppKit.HeaderCell( this, i_Value ); 
		}; 
		
		this.__prototype__.HideHeader = function() {
			this.m_HeaderElement.style.display = "none"; 
		}; 
		
		this.__prototype__.ShowHeader = function() {
			this.m_HeaderElement.style.display = "normal"; 
		}; 
		
		this.__prototype__.Empty = function() {
			this.m_SelectedCells = []; 
			this.m_Rows = []; 
			this.m_BodyElement.innerHTML = ""; 
			
		}; 
		
		this.__prototype__.NumColumns = function() {
			return this.m_Columns.length; 
		}; 
		
		this.__prototype__.NumRows = function() {
			return this.m_Rows.length; 
		}; 
		
		this.__prototype__.SelectSignal = function() {
			return this.m_SelectSignal; 
		}; 
		
		this.__prototype__.DoubleSelectSignal = function() {
			return this.m_DoubleSelectSignal; 
		}; 

		this.__prototype__.ChangeSignal = function() {
			return this.m_ChangeSignal; 
		}; 
		
		this.__prototype__.ScrollBottomSignal = function() {
			return this.m_ScrollBottomSignal; 
		}; 
		
		this.__prototype__.SetIsMultipleSelect = function( i_MultipleSelect ) {
			this.m_MultipleSelect = i_MultipleSelect; 
		}; 
		
		this.__prototype__.IsMultipleSelect = function() {
			return this.m_IsMultipleSelect; 
		}; 
		
		this.__prototype__.SelectCell = function( i_Cell, i_UnSelectOthers ) {
			if( !i_Cell || !(i_Cell instanceof AppKit.Cell) ) {
				throw new Error( "parameter i_Cell for AppKit::Table::SelectCell() is of incorrect type; expecting AppKit.Cell" ); 
			}
			
			if( i_UnSelectOthers || !this.m_IsMultipleSelect ) {
				for( var i = this.m_SelectedCells.length - 1; i > -1; --i ) {
					this.m_SelectedCells[i].UnSelect(); 
					this.m_SelectedCells.splice( i, 1 ); 
				}
				
			}
			
			this.m_SelectedCells.push( i_Cell ); 
			
			this.m_SelectSignal.Emit( [this, i_Cell] ); 
			
		}; 
		
		this.__prototype__.UnSelectCell = function( i_Cell ) {
			for( var i = 0; i < this.m_SelectedCells.length; ++i ) {
				if( this.m_SelectedCells[i] === i_Cell ) {
					this.m_SelectedCells.splice( i, 1 ); 
				}
				
			}
			
		}; 

		this.__prototype__.ChangeCell = function( i_Cell ) {
			this.m_ChangeSignal.Emit( [this, i_Cell] ); 
		}; 
		
		this.__prototype__.DoubleSelectCell = function( i_Cell ) {
			this.m_DoubleSelectSignal.Emit( [this, i_Cell] ); 
		}; 
		
		this.__prototype__.AddColumn = function( i_Value ) {
			this.m_Columns.push( this.CreateHeaderCell( i_Value ) ); 
			
			this.m_HeaderElement.appendChild( this.m_Columns[this.m_Columns.length - 1].Element() ); 
			
			for( var i = 0; i < this.m_Rows.length; ++i ) {
				this.m_Rows[i].push( this.CreateCell() ); 
				this.m_BodyElement.childNodes[i].appendChild( this.m_Rows[i][this.m_Rows[i].length -1].Element() ); 
			}
			
		}; 
		
		this.__prototype__.AddRow = function() {
			this.m_Rows.push( [] ); 
			
			var RowElement = document.createElement( "tr" ); 
			
			for( var i = 0; i < this.m_Columns.length; ++i ) {
				this.m_Rows[this.m_Rows.length - 1].push( this.CreateCell() ); 
				RowElement.appendChild( this.m_Rows[this.m_Rows.length - 1][this.m_Rows[this.m_Rows.length - 1].length -1].Element() ); 
			}
			
			this.m_BodyElement.appendChild( RowElement ); 
			
		}; 
		
		this.__prototype__.GetCell = function( i_RowIndex, i_ColumnIndex ) {
			return this.m_Rows[i_RowIndex][i_ColumnIndex]; 
		}; 
		
		this.__prototype__.HandleScroll = function(e) {
			if( e.target.scrollTop > (e.target.scrollHeight - this.m_Element.offsetHeight) - 200 && this.m_DoEmitScrollBottom ) {
				this.m_DoEmitScrollBottom = false; 
				this.m_ScrollBottomSignal.Emit( [this] ); 
			}
			
		}; 
		
		this.__prototype__.SetDoEmitScrollBottom = function( i_DoEmitScrollBottom ) {
			this.m_DoEmitScrollBottom = i_DoEmitScrollBottom; 
		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-Table" ); 
			
			this.m_TableElement.appendChild( this.m_HeaderElement );
			this.m_TableElement.appendChild( this.m_BodyElement ); 
			
			this.m_Element.appendChild( this.m_TableElement ); 
			this.m_Element.appendChild( this.m_FooterElement ); 
			
			this.m_Element.onscroll = this.HandleScroll.bind(this); 
			
		}
		
	}; 
	
	AppKit.Table.prototype = new AppKit.View;
	
}); 
