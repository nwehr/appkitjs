define(["AppKit/Core/AppKit-Base", "AppKit/View/AppKit-Size", "AppKit/View/AppKit-Position"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.View = function( i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Element : {
				value		: document.createElement( "div" )
				, writable	: true
			}
			, m_Tag : {
				value 		: 0
				, writable 	: true
			}
			, m_Size : {
				value 		: new AppKit.Size( new AppKit.Distance( 0, "px" ), new AppKit.Distance( 0, "px" ) )
				, writable 	: true
			}
			, m_Position : {
				value		: new AppKit.Position( new AppKit.Coordinate( 0, "px" ), new AppKit.Coordinate( 0, "px" ) )
				, writable	: true
			}
			, m_IsResponsive : {
				value 		: false
				, writable 	: true
			}
			, m_IsSelectable : {
				value 		: false
				, writable 	: true
			}
			
		}); 
	
		// Function Members
		this.__prototype__.Destroy = function() {
			if( this.m_Element.parentNode )
				this.m_Element.parentNode.removeChild( this.m_Element ); 
				
			delete this; 
			
		}; 
		
		this.__prototype__.Element = function() {
			return this.m_Element; 
		}; 
		
		this.__prototype__.SetTag = function( i_Tag ) {
			if( i_Tag ) this.m_Tag = i_Tag; 
		}; 
		
		this.__prototype__.Tag = function() {
			return this.m_Tag; 
		}; 
		
		this.__prototype__.SetSize = function( i_Size ) {
			if( !i_Size || !(i_Size instanceof AppKit.Size) )
				throw new Error( "parameter i_Size for AppKit::View::SetSize() is of incorrect type; expecting AppKit.Size" ); 
			
			this.m_Size = i_Size; 
			
			if( this.m_Size.WidthDistance() )
				this.m_Element.style.width 	= this.m_Size.Width(); 

			if( this.m_Size.HeightDistance() )
				this.m_Element.style.height = this.m_Size.Height(); 
			
		}; 
		
		this.__prototype__.Size = function() {
			return this.m_Size; 
		}; 
		
		this.__prototype__.SetPosition = function( i_Position ) {
			if( !i_Position || !(i_Position instanceof AppKit.Position) )
				throw new Error( "parameter i_Position for AppKit::View::SetPosition() is of incorrect type; expecting AppKit.Position" ); 
			
			this.m_Position = i_Position; 
			
			if( this.m_Position instanceof AppKit.RelativePosition ) {
				this.m_Element.style.position = "relative"; 
				
				if( this.m_Position.XCoordinate() )
					this.m_Element.style.left = this.m_Position.X(); 

				if( this.m_Position.YCoordinate() )
					this.m_Element.style.top = this.m_Position.Y(); 
				
			} else if( this.m_Position instanceof AppKit.AbsolutePosition ) {
				this.m_Element.style.position = "absolute"; 
				
				if( this.m_Position.XCoordinate() )
					this.m_Element.style.left = this.m_Position.X(); 

				if( this.m_Position.YCoordinate() )
					this.m_Element.style.top = this.m_Position.Y(); 
				
			} else if( this.m_Position instanceof AppKit.FixedPosition ) {
				this.m_Element.style.position = "fixed"; 
				
				if( this.m_Position.XCoordinate() )
					this.m_Element.style.left = this.m_Position.X(); 

				if( this.m_Position.YCoordinate() )
					this.m_Element.style.top = this.m_Position.Y(); 
				
			} else {
				if( this.m_Position.XCoordinate() )
					this.m_Element.style.marginLeft = this.m_Position.X(); 

				if( this.m_Position.YCoordinate() )
					this.m_Element.style.marginTop = this.m_Position.Y(); 

			}
			
		}; 
		
		this.__prototype__.Position = function() {
			return this.m_Position; 
		}; 
		
		this.__prototype__.Append = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.View) ) {
				throw new Error( "parameter i_View for AppKit::View::Append() is of incorrect type; expecting AppKit.View" ); 
			}
		
			this.m_Element.appendChild( i_View.Element() ); 
			
			return i_View; 
			
		}; 
		
		this.__prototype__.Empty = function() {
			this.m_Element.innerHTML = ""; 
		}; 
		
		this.__prototype__.AddClass = function( i_Class ) {
			this.m_Element.className += " " + i_Class; 
			this.m_Element.className = this.m_Element.className.replace( /^\s+|\s+$/g, "" ); 
		};
		
		this.__prototype__.RemoveClass = function( i_Class ) {
			this.m_Element.className = this.m_Element.className.replace( new RegExp( "\\b\\s?" + i_Class + "\\b" ), "" ).replace( /^\s+|\s+$/g, "" ); 
		};
		
		this.__prototype__.SetIsResponsive = function( i_IsResponsive ) {
			this.m_IsResponsive = i_IsResponsive; 
			
			if( this.m_IsResponsive ) {
				this.AddClass( "AppKit-Responsive" ); 
			} else {
				this.RemoveClass( "AppKit-Responsive" ); 
			}
			
		}; 
		
		this.__prototype__.IsResponsive = function() {
			return this.m_IsResponsive; 
		}; 
		
		this.__prototype__.SetIsSelectable = function( i_IsSelectable ) {
			this.m_IsSelectable = i_IsSelectable; 
			
			if( this.m_IsSelectable ) {
				this.AddClass( "AppKit-Selectable" ); 
			} else {
				this.RemoveClass( "AppKit-Selectable" ); 
			}
			
		}; 
		
		this.__prototype__.IsSelectable = function() {
			return this.m_IsSelectable; 
		}; 
		
		// Constructor
		{
			if( i_Element ) {
				this.m_Element = i_Element; 
			}
			
			this.AddClass( "AppKit-View" ); 
			
			if( i_Size ) 		this.SetSize		( i_Size ); 
			if( i_Position ) 	this.SetPosition	( i_Position ); 
			if( i_Tag ) 		this.SetTag			( i_Tag ); 
			
		}
		
	}; 
		
}); 
