define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Button : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Button = function( i_Text, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "button" ), i_Tag] ); 
	
		// Data Members
		Object.defineProperties(this, {
			m_SelectSignal : {
				value		: new AppKit.Signal()
				, writable	: true
			}
			, m_DownSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			, m_UpSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SelectSignal = function() {
			return this.m_SelectSignal; 
		}; 
		
		this.__prototype__.DownSignal = function() {
			return this.m_DownSignal; 
		}; 
	
		this.__prototype__.UpSignal = function() {
			return this.m_UpSignal; 
		}; 
	
		this.__prototype__.Append = function( i_View ) {}; 
		
		this.__prototype__.SetText = function( i_Text ) {
			this.m_Element.innerHTML = i_Text; 
		}; 
		
		this.__prototype__.HandleClick = function(e) {
			var evt = e || window.event; 
			
			this.m_SelectSignal.Emit( [this] ); 
			
		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-Button" ); 
			
			if( i_Text ) this.SetText( i_Text ); 
			
			this.m_Element.onclick 		= function(e) { this.m_SelectSignal.Emit( [this] ); }.bind(this); 
			this.m_Element.onmousedown 	= function(e) { this.m_DownSignal.Emit( [this] );  }.bind(this); 
			this.m_Element.onmouseup 	= function(e) { this.m_UpSignal.Emit( [this] ); }.bind(this); 
	
		}
		
	};
	
	AppKit.Button.prototype = new AppKit.View; 
	
	///////////////////////////////////////////////////////////////////////////////
	// DropdownSelector : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.DropdownSelector = function( i_Text, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [null, null, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_SelectSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetText = function( i_Text ) {
			this.m_Element.innerHTML = i_Text; 
		}; 
		
		this.__prototype__.SelectSignal = function() {
			return this.m_SelectSignal; 
		}; 
		
		// Constructor 
		{
			if( i_Text ) this.SetText( i_Text ); 
			
			//this.m_Element.onmouseup 	= function() { this.m_SelectSignal.Emit( [this] ); }.bind(this); 
			this.m_Element.onclick 		= function() { this.m_SelectSignal.Emit( [this] ); }.bind(this); 
			
			this.m_Element.className += " AppKit-DropdownSelector"; 
			
		}
		
	};
	
	AppKit.DropdownSelector.prototype = new AppKit.View; 

	///////////////////////////////////////////////////////////////////////////////
	// DropdownButton : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.DropdownButton = function( i_Text, i_Selectors, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_LHContainerElement : {
				value 		: document.createElement( "div" )
				, writable	: true
			}
			, m_RHContainerElement : {
				value 		: document.createElement( "div" )
				, writable 	: true
			}
			, m_DropdownElement : {
				value 		: document.createElement( "div" )
				, writable 	: true
			}
			, m_ArrowElement : {
				value 		: document.createElement( "div" )
				, writable 	: true
			}
			, m_Selectors : {
					value 		: []
				, writable 	: true
			}
			, m_SelectorSlots : {
				value 		: []
				, writable 	: true
			}
			, m_SelectorsVisible : {
				value 		: false
				, writable 	: true
			}
			
		}); 
		
		// Private Function Members
		var _Press = function() {
			this.m_RHContainerElement.style.background = "-webkit-gradient( linear, left top, left bottom, color-stop(0%, #bbb), color-stop(100%, #c1c1c1) )"; 
			this.m_RHContainerElement.style.borderLeft = "solid 0.1em #aaa"; 
			
			if( this.m_DropdownElement.style.display == "block" ) _HideSelectors.call( this ); 
			else _ShowSelectors.call( this ); 
			
		}; 
		
		var _Unpress = function() {
			this.m_RHContainerElement.style.background = "none"; 
			this.m_RHContainerElement.style.borderLeft = "solid 0.1em white"; 
		}; 
		
		var _ShowSelectors = function() {
			this.m_DropdownElement.style.display = "block"; 
		}; 
		
		var _HideSelectors = function() {
			this.m_DropdownElement.style.display = "none"; 
		}; 
		
		// Function Members
		this.__prototype__.SetSelectors = function( i_Selectors ) {
			if( !i_Selectors || !(i_Selectors instanceof Array) ) {
				throw new Error( "parameter i_Selectors for AppKit::DropdownButton::SetSelectors() is of incorrect type; expecting Array" ); 
			}
			
			this.m_Selectors 		= []; 
			this.m_SelectorSlots 	= []; 
			
			this.m_DropdownElement.innerHTML = ""; 
			
			for( var i = 0; i < i_Selectors.length; ++i ) {
				if( !(i_Selectors[i] instanceof AppKit.DropdownSelector) ) {
					throw new Error( "parameter member for AppKit::DropdownButton::SetSelectors() is of incorrect type; expecting AppKit.DropdownSelector" ); 
				}
				
				var SelectorSlot = new AppKit.Slot( _HideSelectors, this ); 
				
				i_Selectors[i].SelectSignal().Connect( SelectorSlot ); 
				
				this.m_Selectors.push( i_Selectors[i] ); 
				this.m_SelectorSlots.push( SelectorSlot ); 
				
				this.m_DropdownElement.appendChild( i_Selectors[i].Element() ); 
				
			}
			
		}; 
		
		this.__prototype__.Selectors = function() {
			return this.m_Selectors; 
		}; 
		
		this.__prototype__.SetText = function( i_Text ) {
			this.m_LHContainerElement.innerHTML = i_Text; 
		}; 
		
		this.__prototype__.Text = function() {
			return this.m_LHContainerElement.innerHTML; 
		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-DropdownButton" ); 
			
			if( i_Text ) this.SetText( i_Text );
			if( i_Selectors ) this.SetSelectors( i_Selectors ); 
			
			this.m_RHContainerElement.appendChild( this.m_ArrowElement ); 
			
			this.m_Element.appendChild( this.m_DropdownElement ); 
			this.m_Element.appendChild( this.m_LHContainerElement ); 
			this.m_Element.appendChild( this.m_RHContainerElement ); 
			
			this.m_RHContainerElement.onmousedown 	= _Press.bind(this); 
			this.m_RHContainerElement.onmouseup 	= _Unpress.bind(this); 
			this.m_RHContainerElement.onmouseout 	= _Unpress.bind(this); 
			
		}
		
	}; 
	
	AppKit.DropdownButton.prototype = new AppKit.View; 
	
}); 
