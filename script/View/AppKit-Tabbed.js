define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Tab : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Tab = function( i_Title, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "li" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Tabbed : {
				value 		: null
				, writable 	: true
			}
			, m_ContentElement : {
				value 		: document.createElement( "div" )
				, writable 	: true
			}
			, m_IsSelected : {
				value 		: false
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Destroy = function() {
			if( this.m_Tabbed ) 
				this.m_Tabbed.RemoveTab( this );
			
			delete this; 
			
		}; 
		
		this.__prototype__.TitleElement = function() {
			return this.m_TitleElement; 
		}; 
		
		this.__prototype__.ContentElement = function() {
			return this.m_ContentElement; 
		}; 
		
		this.__prototype__.Append = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.View) ) {
				throw new Error( "parameter i_View for AppKit::Tab::Append() is of incorrect type; expecting AppKit.View" ); 
			}
			
			this.m_ContentElement.appendChild( i_View.Element() ); 
			
		}; 
		
		this.__prototype__.SetTitle = function( i_Text ) {
			this.m_Element.innerHTML = i_Text; 
		}; 
		
		this.__prototype__.Title = function() {
			return this.m_Element.innerHTML; 
		}; 
		
		this.__prototype__.SetTabbed = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.Tabbed) ) {
				throw new Error( "parameter i_View for AppKit::Tab::SetTabbed() is of incorrect type; expecting AppKit.Tabbed" ); 
			}
			
			this.m_Tabbed = i_View; 
			
		}; 
		
		this.__prototype__.Select = function() {
			this.m_IsSelected = false; 
			this.AddClass( "AppKit-Selected" ); 
			
			this.m_Tabbed.SelectTab( this ); 
			
		}; 
		
		this.__prototype__.UnSelect = function() {
			this.m_IsSelected = false; 
			this.RemoveClass( "AppKit-Selected" ); 
		}; 
		
		this.__prototype__.IsSelected = function() {
			return this.m_IsSelected; 
		}; 
		
		this.__prototype__.HandleSelect = function(e) {
			if( !this.m_IsSelected ) 
				this.Select(); 
			
		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-Tab" ); 
			
			this.m_Element.onclick = this.HandleSelect.bind(this); 
			
			if( i_Title ) this.SetTitle( i_Title ); 
			
		}
		
	}
	
	AppKit.Tab.prototype = new AppKit.View; 
	
	
	///////////////////////////////////////////////////////////////////////////////
	// MutableTab : Tab
	///////////////////////////////////////////////////////////////////////////////
	AppKit.MutableTab = function( i_Title, i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Tab.apply( this, [null, i_Size, i_Position, i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_CloseElement : {
				value 		: document.createElement( "span" )
				, writable 	: true
			}
			, m_TitleElement : {
				value 		: document.createElement( "span" )
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetTitle = function( i_Text ) {
			this.m_TitleElement.innerHTML = i_Text; 
		}; 
		
		this.__prototype__.Title = function() {
			return this.m_TitleElement.innerHTML; 
		}; 
		
		this.__prototype__.HandleClose = function(e) {
			if( !e ) e = window.event;
			
			if( e.stopPropagation ) e.stopPropagation();
			else e.cancelBubble = true;
			
			this.Destroy(); 
			
		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-MutableTab" ); 
			
			this.m_CloseElement.onclick = this.HandleClose.bind(this); 
			this.m_CloseElement.innerHTML = "&#8855;"; 
			
			this.m_Element.appendChild( this.m_CloseElement ); 
			this.m_Element.appendChild( this.m_TitleElement ); 
			
			if( i_Title ) this.SetTitle( i_Title ); 
			
		}
		
	}
	
	AppKit.MutableTab.prototype = new AppKit.Tab; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Tabbed : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Tabbed = function( i_Size, i_Position, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this, [i_Size, i_Position, document.createElement( "div" ), i_Tag] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_TabTitleContainerElement : {
				value 		: document.createElement( "ul" )
				, writable 	: true
			}
			, m_TabContentContainerElement : {
				value 		: document.createElement( "div" )
				, writable 	: true
			}
			, m_Tabs : {
				value 		: []
				, writable 	: true
			}
			, m_SelectedTab : {
				value 		: null
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Append = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.Tab) ) {
				throw new Error( "parameter i_View for AppKit::Tabbed::AddTab() is of incorrect type; expecting AppKit.Tab" ); 
			}
			
			this.m_TabTitleContainerElement.appendChild( i_View.Element() ); 
			
			i_View.SetTabbed( this ); 
			this.m_Tabs.push( i_View ); 
			
			if( !this.m_SelectedTab ) i_View.Select(); 
			
		}; 
		
		this.__prototype__.RemoveTab = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.Tab) ) {
				throw new Error( "parameter i_View for AppKit::Tabbed::RemoveTab() is of incorrect type; expecting AppKit.Tab" ); 
			}
			
			for( var i = 0; i < this.m_Tabs.length; ++i ) {
				if( this.m_Tabs[i] === i_View ) {
					this.m_TabTitleContainerElement.removeChild( this.m_Tabs[i].Element() ); 
					this.m_Tabs.splice( i, 1 ); 
				}
				
			}
			
			if( i_View === this.m_SelectedTab && this.m_Tabs.length )
				this.m_Tabs[0].Select(); 
			
		}; 
		
		this.__prototype__.SelectTab = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.Tab) ) {
				throw new Error( "parameter i_View for AppKit::Tabbed::SelectTab() is of incorrect type; expecting AppKit.Tab" ); 
			}
			
			
			if( this.m_SelectedTab ) 
				this.m_SelectedTab.UnSelect();
			
			this.m_SelectedTab = i_View; 
			
			this.m_TabContentContainerElement.innerHTML = ""; 
			this.m_TabContentContainerElement.appendChild( this.m_SelectedTab.ContentElement() ); 
			
		}; 
		
		// Constructor
		{
			this.AddClass( "AppKit-Tabbed" ); 
			
			this.m_Element.appendChild( this.m_TabTitleContainerElement ); 
			this.m_Element.appendChild( this.m_TabContentContainerElement ); 
			
		}
		
	}
	
	AppKit.Tabbed.prototype = new AppKit.View; 
	
}); 