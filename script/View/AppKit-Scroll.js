define(["AppKit/Core/AppKit-Base", "AppKit/View/AppKit-Size", "AppKit/View/AppKit-Position", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Scroll : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Scroll = function( i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this );
		
		AppKit.View.apply( this, [ i_Size, i_Position, i_Element, i_Tag ] );
		
		// Data Members
		Object.defineProperties(this, {
			m_ScrollElement : {
				value : document.createElement( "div" )
				, writable : true
			}
		}); 
	
		// Function Members
		this.__prototype__.Append = function( i_View ) {
			if( !i_View || !(i_View instanceof AppKit.View) ) {
				throw new Error( "parameter i_View for AppKit::View::Append() is of incorrect type; expecting AppKit.View" ); 
			}
		
			this.m_ScrollElement.appendChild( i_View.Element() ); 
			
			return i_View; 
			
		}; 
		
		this.__prototype__.Empty = function() {
			this.m_ScrollElement.innerHTML = ""; 
		}; 
		
		// Constructor
		{
			this.m_Element.className += " AppKit-Scroll"; 
			this.m_Element.appendChild ( this.m_ScrollElement );
		}
		
	}; 
	
	AppKit.Scroll.prototype = new AppKit.View;
		
}); 
