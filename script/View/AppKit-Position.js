define(["AppKit/Core/AppKit-Base"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Coordinate
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Coordinate = function( i_Coord, i_Unit ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Coord : {
				value		: 0
				, writable	: true
			}
			, m_Unit : {
				value		: "px"
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetCoord = function( i_Coord ) {
			if( i_Coord ) this.m_Coord = i_Coord; 
		}; 
		
		this.__prototype__.Coord = function() {
			return this.m_Coord; 
		}; 
		
		this.__prototype__.SetUnit = function( i_Unit ) {
			if( i_Unit ) this.m_Unit = i_Unit; 
		}; 
		
		this.__prototype__.Unit = function() {
			return this.m_Unit; 
		}; 
		
		// Constructor
		{
			if( i_Coord ) 	this.SetCoord 	( i_Coord ); 
			if( i_Unit ) 	this.SetUnit 	( i_Unit ); 
		}
		
	}; 
	
	///////////////////////////////////////////////////////////////////////////////
	// Position
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Position = function( i_XCoordinate, i_YCoordinate ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_XCoordinate : {
				value		: new AppKit.Coordinate( 0, "px" )
				, writable	: true
			}
			, m_YCoordinate : {
				value		: new AppKit.Coordinate( 0, "px" )
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetXCoordinate = function( i_XCoordinate ) {
			if( i_XCoordinate && i_XCoordinate instanceof AppKit.Coordinate )
				this.m_XCoordinate = i_XCoordinate; 
			
		}; 
		
		this.__prototype__.XCoordinate = function() {
			return this.m_XCoordinate; 
		}; 
		
		this.__prototype__.X = function() {
			return (this.m_XCoordinate.Coord() + this.m_XCoordinate.Unit()); 
		}; 
		
		this.__prototype__.SetYCoordinate = function( i_YCoordinate ) {
			if( i_YCoordinate && i_YCoordinate instanceof AppKit.Coordinate )
				this.m_YCoordinate = i_YCoordinate; 
			
		}; 
		
		this.__prototype__.YCoordinate = function() {
			return this.m_YCoordinate; 
		}; 
		
		this.__prototype__.Y = function() {
			return (this.m_YCoordinate.Coord() + this.m_YCoordinate.Unit()); 
		}; 
		
		// Constructor
		{
			if( i_XCoordinate ) this.SetXCoordinate( i_XCoordinate ); 
			if( i_YCoordinate ) this.SetYCoordinate( i_YCoordinate ); 
		}
		
	}; 
	
	///////////////////////////////////////////////////////////////////////////////
	// RelativePosition : Position
	///////////////////////////////////////////////////////////////////////////////
	AppKit.RelativePosition = function( i_XCoordinate, i_YCoordinate ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Position.apply( this, [i_XCoordinate, i_YCoordinate] ); 
		
	}; 
	
	AppKit.RelativePosition.prototype = new AppKit.Position; 
	
	///////////////////////////////////////////////////////////////////////////////
	// AbsolutePosition : Position
	///////////////////////////////////////////////////////////////////////////////
	AppKit.AbsolutePosition = function( i_XCoordinate, i_YCoordinate ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Position.apply( this, [i_XCoordinate, i_YCoordinate] ); 
		
	}; 
	
	AppKit.AbsolutePosition.prototype = new AppKit.Position; 
	
	///////////////////////////////////////////////////////////////////////////////
	// FixedPosition : Position
	///////////////////////////////////////////////////////////////////////////////
	AppKit.FixedPosition = function( i_XCoordinate, i_YCoordinate ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Position.apply( this, [i_XCoordinate, i_YCoordinate] ); 
		
	}; 
	
	AppKit.FixedPosition.prototype = new AppKit.Position; 
	
}); 
