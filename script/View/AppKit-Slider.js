define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Slider : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Slider = function( i_MinimumValue, i_MaximumValue, i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this
			, [(i_Size 
					? i_Size 
					: new AppKit.Size( new AppKit.Distance( 200, "px" )
				, new AppKit.Distance( 10, "px" ) ))
				, i_Position
				, i_Element
				, i_Tag
			] 
		); 
		
		// Private Data Members
		var m_DragActive = false; 
		var m_DragOffset = 0; 
	
		// Public Data Members
		Object.defineProperties(this, {
			// DOM Elements
			m_SliderElement : {
				value		: document.createElement( "div" )
				, writbale 	: true
			}
			, m_KnobElement : {
				value		: document.createElement( "a" ) 
				, writable	: true
			}
			// Signals
			, m_SlideSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			, m_ValueSignal : {
				value 		: new AppKit.Signal()
				, writable 	: true
			}
			// Values
			, m_MinimumValue : {
				value		: 0
				, writable 	: true
			}
			, m_MaximumValue : {
				value		: 100
				, writable	: true
			}
			, m_CurrentValue : {
				value 		: 0
				, writable 	: true
			}
			
		}); 
		
		// Private Function Members
		var _Slide = function( e ) {
			if( m_DragActive ){
				var ElementOffsetL = this.m_Element.offsetLeft; 
				
				var PointerX 	= window.event.clientX - ElementOffsetL; 
				var KnobX 		= (PointerX - m_DragOffset); 
				
				if( KnobX < -1 )
					KnobX = -2; 
				
				if( KnobX > this.m_SliderElement.offsetWidth - 11 )
					KnobX = this.m_SliderElement.offsetWidth - 12; 
				
				this.m_KnobElement.style.left = KnobX + "px"; 
				
				var PaddingLeftPX 	= window.getComputedStyle( this.m_Element, null ).getPropertyValue('padding-left'); 
				var PaddingLeft 	= PaddingLeftPX.substr( 0, PaddingLeftPX.length - 2 ); 
				
				this.m_CurrentValue = this.m_MinimumValue + (((KnobX + 2) * (this.m_MaximumValue - this.m_MinimumValue)) / (this.m_SliderElement.offsetWidth - PaddingLeft)); 
				
				this.m_SlideSignal.Emit( [this, this.m_CurrentValue] ); 
	
			}
				
		}; 
		
		var _Grab = function( e ) {
			m_DragActive = true; 
			
			var ElementOffset 	= this.m_Element.offsetLeft; 
			var KnobPosition 	= Number( this.m_KnobElement.style.left.substr( 0, this.m_KnobElement.style.left.length - 2) ); 
			
			m_DragOffset = (window.event.clientX - ElementOffset) - KnobPosition;
			
			document.body.addEventListener( "mousemove", _Slide.bind(this), true ); 
			document.body.addEventListener( "mouseup", _UnGrab.bind(this), true ); 
			
		}; 
		
		var _UnGrab = function( e ) {
			m_DragActive = false; 
			
			document.body.removeEventListener( "mousemove", _Slide.bind(this) ); 
			document.body.removeEventListener( "mouseup", _UnGrab.bind(this) ); 
	
			this.m_ValueSignal.Emit( [this, this.m_CurrentValue] ); 
	
		}; 
	
		// Public Function Members & Inheritance
		this.__prototype__.Value = function() {
			return this.m_CurrentValue; 
		}; 
	
		this.__prototype__.SetValue = function( i_Value ) {
			
		}; 
		
		this.__prototype__.SlideSignal = function() {
			return this.m_SliderSignal; 
		}; 
	
		this.__prototype__.ValueSignal = function() {
			return this.m_ValueSignal; 
		}
		
		this.__prototype__.Append = function( i_View ) {}; 
		
		// Constructor
		{
			this.m_KnobElement.onmousedown 	= _Grab.bind( this ); 
			
			this.m_KnobElement.className = "AppKit-Slider-Knob"; 
			
			this.m_SliderElement.className = "AppKit-Slider"; 
			this.m_SliderElement.appendChild( this.m_KnobElement ); 
			
			this.m_Element.className += " AppKit-Slider-Zone"; 
			this.m_Element.appendChild	( this.m_SliderElement ); 
			
			if( i_MinimumValue ) this.m_MinimumValue = i_MinimumValue; 
			if( i_MaximumValue ) this.m_MaximumValue = i_MaximumValue; 
			
		}
		
	}; 
	
	AppKit.Slider.prototype = new AppKit.View; 
	
}); 
