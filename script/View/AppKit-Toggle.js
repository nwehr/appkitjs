define(["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/View/AppKit-View"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Toggle : View
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Toggle = function( i_State, i_Size, i_Position, i_Element, i_Tag ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.View.apply( this
			, [new AppKit.Size( new AppKit.Distance( 60, "px" )
				, new AppKit.Distance( 20, "px" ) )
				, i_Position
				, i_Element
				, i_Tag
			] 	
		); 
		
		// Private Data Members
		var m_SliderInterval 	= null; 
	
		// Public Data Members
		Object.defineProperties(this, {
			// DOM Elements
			m_SliderElement : {
				value 		: document.createElement( "div" )
				, writable	: true
			}
			// Signals
			, m_ToggleSignal : {
				value 		: new AppKit.Signal()
				, writbale 	: true
			}
			// Values
			, m_State : {
				value 		: false
				, writable 	: true
			}
			
		}); 
		
		// Private Function Members
		var _SlideLeft = function() {
			if( Number( this.m_SliderElement.style.left.substring(0, this.m_SliderElement.style.left.length - 2) ) == 0 )
				clearInterval( m_SliderInterval ); 
			else {
				var Left = Number( this.m_SliderElement.style.left.substring(0, this.m_SliderElement.style.left.length - 2) ); 
				this.m_SliderElement.style.left = (Left - 5) + "px"; 
			}
	
		}; 
	
		var _SlideRight = function() {
			if( Number( this.m_SliderElement.style.left.substring(0, this.m_SliderElement.style.left.length - 2) ) == 25 )
				clearInterval( m_SliderInterval ); 
			else {
				var Left = Number( this.m_SliderElement.style.left.substring(0, this.m_SliderElement.style.left.length - 2) ); 
				this.m_SliderElement.style.left = (Left + 5) + "px"; 
			}
			
		}; 
	
		// Public Function Members & Inheritance
		this.__prototype__.ToggleSignal = function() {
			return this.m_ToggleSignal; 
		}; 
	
		this.__prototype__.State = function() {
			return this.m_State; 
		}; 
	
		this.__prototype__.ToggleState = function() {
			this.m_State = !this.m_State; 
			
			if( this.m_State )
				m_SliderInterval = setInterval( _SlideRight.bind( this ), 10 ); 
			else
				m_SliderInterval = setInterval( _SlideLeft.bind( this ), 10 ); 
			
			this.m_ToggleSignal.Emit( [this, this.m_State] );
			
		}; 
		
		this.__prototype__.SetState = function( i_State ) {
			if( i_State != this.m_State ) {
				this.m_State = i_State; 
				
				this.m_SliderElement.style.left = (this.m_State ? "25px" : "0px"); 
	
				this.m_ToggleSignal.Emit( [this, this.m_State] ); 
	
			}
			
		}; 
		
		this.__prototype__.Append = function( i_View ) {}; 
		
		// Constructor
		{
			this.m_Element.className 		+= " AppKit-Toggle"; 
			this.m_SliderElement.className 	= "AppKit-Toggle-Slider"; 
			
			var SpanOn 	= document.createElement( "span" ); 
			var SpanOff	= document.createElement( "span" ); 
			
			SpanOn.innerText 	= "on"; 
			SpanOff.innerText 	= "off";
			
			var BGOn 	= document.createElement( "div" ); 
			var BGOff 	= document.createElement( "div" ); 
			
			BGOn.className = "AppKit-Toggle-On"; 
			BGOn.appendChild( SpanOn ); 
			
			BGOff.className = "AppKit-Toggle-Off"; 
			BGOff.appendChild( SpanOff ); 
			
			this.m_Element.appendChild( BGOn ); 
			this.m_Element.appendChild( BGOff ); 
			this.m_Element.appendChild( this.m_SliderElement ); 
			
			this.m_SliderElement.onclick = function() { this.ToggleState(); }.bind(this); 
	
			if( i_State ) this.SetState( i_State ); 
			
		}
		
	}; 
	
	AppKit.Toggle.prototype = new AppKit.View; 
	
}); 
