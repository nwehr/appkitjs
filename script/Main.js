//
// Copyright 2013 Nathan Wehr and EvriChart Inc. All Rights Reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
//     1. Redistributions of source code must retain the above copyright notice, this list of
//     conditions and the following disclaimer.
//
//     2. Redistributions in binary form must reproduce the above copyright notice, this list
//     of conditions and the following disclaimer in the documentation and/or other materials
//     provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those of the
// authors and should not be interpreted as representing official policies, either expressed
// or implied, of Nathan Wehr.
// 

window.onerror = function( errorMsg, url, lineNumber ) {
	if( window.MyApp ) {
		window.ErrorWindow = window.MyApp.MainWindow().AddChildWindow( new AppKit.Window( "Uncaught Error"
				, null
				, AppKit.MakeSize( 350, "px" )
				, AppKit.MakeAbsolutePosition( (window.innerWidth / 2) - 150, "px", 70, "px" ) 
			)
			, true 
		); 

		ErrorWindow.Append( new AppKit.Paragraph( errorMsg ) ); 
		ErrorWindow.Append( new AppKit.Paragraph( url + ":" + lineNumber ) ); 

		return true; 

	}

}; 

function Main( args ) {
	require.config({baseUrl: "/", paths: {AppKit:"script"}});

	require(
		[
			"AppKit/Core/AppKit-Base"
			, "AppKit/Core/AppKit-SignalSlot"
			, "AppKit/Core/AppKit-Network"
			, "AppKit/Model/AppKit-Model"
			, "AppKit/Model/AppKit-RadioModel"
			, "AppKit/Model/AppKit-TableModel"
			, "AppKit/Model/AppKit-TreeModel"
			, "AppKit/Model/AppKit-HTTPTableModel"
			, "AppKit/View/AppKit-Size"
			, "AppKit/View/AppKit-Position"
			, "AppKit/View/AppKit-View"
			, "AppKit/View/AppKit-Scroll"
			, "AppKit/View/AppKit-Tree"
			, "AppKit/View/AppKit-Text"
			, "AppKit/View/AppKit-Input"
			, "AppKit/View/AppKit-Button"
			, "AppKit/View/AppKit-Toggle"
			, "AppKit/View/AppKit-ToolTip"
			, "AppKit/View/AppKit-Slider"
			, "AppKit/View/AppKit-Tabbed"
			, "AppKit/View/AppKit-Table"
			, "AppKit/View/AppKit-TitleBar"
			, "AppKit/View/AppKit-Window"
			, "AppKit/Controller/AppKit-Controller"
			, "AppKit/Controller/AppKit-TableController"
			, "AppKit/Controller/AppKit-TreeController"
			, "AppKit/Controller/AppKit-RadioController"
			, "AppKit/AppKit-Application"
		]
		, function(){
			try {
				window.MyApp = new AppKit.Application( "My First Application", document.body ); 
				
				// Dropdown
				{
					var ModalSelector 		= new AppKit.DropdownSelector( "Modal" ); 
					var NonModalSelector 	= new AppKit.DropdownSelector( "Non Modal" ); 
					
					ModalSelector.SelectSignal().Connect( new AppKit.Slot( function() {
						MyApp.MainWindow().AddChildWindow( new AppKit.Window( "Modal Window"
								, null
								, AppKit.MakeSize( {"px":300}, {"px":150} )
								, AppKit.MakeAbsolutePosition( (window.innerWidth / 2) - 150, "px", 70, "px" ) 
							)
							, true 
						); 
					}, MyApp ) ); 
					
					NonModalSelector.SelectSignal().Connect( new AppKit.Slot( function() {
						MyApp.MainWindow().AddChildWindow( new AppKit.Window( "Non Modal Window"
								, null
								, AppKit.MakeSize( {"px":300}, {"px":150} )
								, AppKit.MakeAbsolutePosition( (window.innerWidth / 2) - 150, "px", 70, "px" ) 
							)
							, false 
						); 
					}, MyApp ) ); 
					
					window.MyDropdown = new AppKit.DropdownButton( "New Window", [ModalSelector, NonModalSelector], AppKit.MakeSize( {"em":8.5}, {"em":1.9} ) ); 
					
					MyApp.MainWindow().Append( MyDropdown ); 
					
				}
				
				MyApp.MainWindow().ContentElement().appendChild( document.createElement( "br" ) ); 
				MyApp.MainWindow().ContentElement().appendChild( document.createElement( "br" ) ); 
				
				// Tree View
				{
					var MyTreeModel = new AppKit.GeneralTreeModel(); 
				
					{
						var Branch1 = MyTreeModel.Data().SetChild( new AppKit.GeneralTreeNode( ["Branch 1"] ) ); 
						var Leaf11 = Branch1.SetChild( new AppKit.GeneralTreeNode( ["Leaf 1.1"] ) ); 
						
						var Branch12 = Leaf11.SetSibling( new AppKit.GeneralTreeNode( ["Branch 1.2"] ) ); 
						var Leaf121 = Branch12.SetChild( new AppKit.GeneralTreeNode( ["Leaf 1.2.1"] ) ); 
						var Leaf122 = Leaf121.SetSibling( new AppKit.GeneralTreeNode( ["Leaf 1.2.2"] ) ); 
					}
					
					window.MyTreeController = new AppKit.TreeController( new AppKit.Tree( AppKit.MakeSize( {"em":20}, {"em":30} ) ), MyTreeModel ); 
					
					MyTreeController.RefreshView(); 					
					
					MyTreeController.DoubleSelect = function( i_ViewNode, i_ModelNode ) {
						if( i_ModelNode.IsLeaf() ) {
							console.log( i_ModelNode.ValueAtIndex( 0 ) ); 
						}
					}.bind(MyTreeController); 
					
					MyTreeController.View().Element().style.display = "inline-block"; 
					
					MyApp.MainWindow().Append( MyTreeController.View() ); 
					
				}

				// Table View
				{
					var MyTableView 	= new AppKit.Table( AppKit.MakeSize( {"em":20}, {"em":30} ) ); 
					var MyTableModel 	= new AppKit.HTTPTableModel( new AppKit.Network.HTTPClient(), "/http/AppKit-ExampleModel.php" ); 
					
					MyTableView.CreateCell = function( i_Value ) {
						return new AppKit.MutableCell( this, i_Value ); 
					}.bind(MyTableView); 

					window.MyTableController = new AppKit.TableController( MyTableView, MyTableModel, ["upper","lower"] ); 
					
					MyTableController.View().AddColumn( "Upper" ); 
					MyTableController.View().AddColumn( "Lower" ); 

					MyTableController.RefreshView();
					
					MyTableController.View().Element().style.display 	= "inline-block"; 
					MyTableController.View().Element().style.marginLeft = "1em"; 
					
					MyApp.MainWindow().Append( MyTableController.View() ); 
					
				}
				
				// Tabbed View
				{
					window.MyTabbedView = new AppKit.Tabbed( AppKit.MakeSize( {"em":20}, {"em":30} ) ); 
					
					var MyTabOne = new AppKit.Tab( "One" ); 
					var MyTabTwo = new AppKit.Tab( "Two" ); 
					var MyTabThree = new AppKit.MutableTab( "Three" ); 
					
					MyTabOne.Append( new AppKit.Paragraph( "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." ) ); 
					MyTabTwo.Append( new AppKit.Paragraph( "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." ) ); 
					MyTabThree.Append( new AppKit.Paragraph( "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." ) ); 
					
					MyTabbedView.Append( MyTabOne ); 
					MyTabbedView.Append( MyTabTwo ); 
					MyTabbedView.Append( MyTabThree ); 
					
					MyTabbedView.Element().style.display 		= "inline-block"; 
					MyTabbedView.Element().style.marginLeft 	= "1em"; 
					MyTabbedView.Element().style.verticalAlign 	= "top"; 
					
					MyApp.MainWindow().Append( MyTabbedView ); 
					
				}
				
			} catch( e ) {
				AppKit.LOG( AppKit.LOG_SEVERITY.ERROR, "AppKit Exception Following:" ); 
				AppKit.LOG( AppKit.LOG_SEVERITY.ERROR, e.message ); 
				AppKit.LOG( AppKit.LOG_SEVERITY.ERROR, e.stack ); 
								
			}
			
		} 
		
	); 
	
	return true; 
	
}
