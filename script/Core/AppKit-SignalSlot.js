define(["AppKit/Core/AppKit-Base"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Signal
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Signal = function() {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Slots : {
				value		: []
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Connect = function( i_Slot ) {
			if( !(i_Slot instanceof AppKit.Slot) ) {
				throw new Error( "parameter i_Slot for AppKit::Signal::Connect() is of incorrect type; expecting AppKit.Slot" ); 
			}
			
			this.m_Slots.push( i_Slot ); 
			
		}; 
		
		this.__prototype__.Disconnect = function( i_Slot ) {
			if( !(i_Slot instanceof AppKit.Slot) ) {
				throw new Error( "parameter i_Slot for AppKit::Signal::Disconnect() is of incorrect type; expecting AppKit.Slot" ); 
			}
			
			for( var i = 0; i < this.m_Slots.length; ++i ) {
				if( i_Slot === this.m_Slots[i] ) {
					this.m_Slots.splice( i, 1 ); 
				}
				
			}
			
		}; 
		
		this.__prototype__.Emit = function( i_Args ) {
			for( var i = 0; i < this.m_Slots.length; ++i ) {
				this.m_Slots[i].Execute( i_Args ); 
			}
			
		}; 
		
		// Constructor
		{}
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	// Slot
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Slot = function( i_Selector, i_Context ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Selector : {
				value		: null
				, writable	: true
			}
			, m_Context : {
				value		: this
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetSelector = function( i_Selector ) {
			this.m_Selector = i_Selector; 
		}; 
		
		this.__prototype__.SetContext = function( i_Context ) {
			this.m_Context = i_Context; 
		}; 
		
		this.__prototype__.Execute = function( i_Args ) {
			this.m_Selector.call( this.m_Context, i_Args ); 
		}; 
		
		// Constructor
		{
			if( i_Selector )  	this.SetSelector( i_Selector ); 
			if( i_Context ) 	this.SetContext	( i_Context ); 
		}
		
	}; 
	
}); 
