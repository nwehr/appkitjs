define(["AppKit/Core/AppKit-Base"], function(){
	AppKit.Network = {
		///////////////////////////////////////////////////////////////////////////////
		// HTTPClient
		///////////////////////////////////////////////////////////////////////////////
		HTTPClient : function( i_Host, i_Port ) {
			this.__prototype__ = Object.getPrototypeOf( this ); 
			
			// DataMembers
			Object.defineProperties(this, {
				m_Host : {
					value 		: ""
					, writable	: true
				}
				, m_Port : {
					value 		: ""
					, writable	: true
				}
				, m_HEADSignal : {
					value 		: new AppKit.Signal()
					, writable	: true
				}
				, m_GETSignal : {
					value 		: new AppKit.Signal()
					, writable	: true
				}
				, m_POSTSignal : {
					value 		: new AppKit.Signal()
					, writable	: true
				}
				, m_PUTSignal : {
					value 		: new AppKit.Signal()
					, writable	: true
				}
				, m_DELETESignal : {
					value 		: new AppKit.Signal()
					, writable 	: true
				}
			}); 
			
			// Function Members
			this.__prototype__.Host = function() {
				return m_Host; 
			}; 
			
			this.__prototype__.Port = function() {
				return m_Port; 
			}; 
			
			this.__prototype__.HEADSignal = function() {
				return this.m_HEADSignal; 
			}; 
			
			this.__prototype__.GETSignal = function() {
				return this.m_GETSignal; 
			}; 
			
			this.__prototype__.POSTSignal = function() {
				return this.m_POSTSignal; 
			}
			
			this.__prototype__.PUTSignal = function() {
				return this.m_PUTSignal; 
			}
			
			this.__prototype__.DELETESignal = function() {
				return this.m_DELETESignal; 
			}; 
			
			this.__prototype__.NewXMLHttp = function( i_Signal ) {
				var XMLHttp = (window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" ));
				
				XMLHttp.onreadystatechange = function() {
					if( XMLHttp.readyState == 4 ) {
						AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Network::HTTPClient::NewXMLHttp() (state change) - debug - response with status " + XMLHttp.status ); 
						AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::Network::HTTPClient::NewXMLHttp() (state change) - trace - response with data " + XMLHttp.responseText ); 
						
						i_Signal.Emit( [XMLHttp] ); 
						
					}
					
				}.bind(this);
				
				return XMLHttp; 
		
			}; 
			
			this.__prototype__.HEAD = function( i_URI ) {
				var XMLHttp = this.NewXMLHttp( this.m_HEADSignal ); 
				
				var URL = this.m_Host + ":" + this.m_Port + i_URI; 
				
				AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Network::HTTPClient::HEAD() - debug - sending HEAD request to " + URL ); 
				
				XMLHttp.open( "HEAD", URL, true ); 
				XMLHttp.send(); 
				
			}; 
			
			this.__prototype__.GET = function( i_URI ) {
				var XMLHttp = this.NewXMLHttp( this.m_GETSignal ); 
				
				var URL = this.m_Host + ":" + this.m_Port + i_URI; 
				
				AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Network::HTTPClient::GET() - debug - sending GET request to " + URL ); 
				
				XMLHttp.open( "GET", URL, true ); 
				XMLHttp.send(); 
				
			}; 
			
			this.__prototype__.POST = function( i_URI, i_Data, i_ContentType ) {
				var XMLHttp = this.NewXMLHttp( this.m_POSTSignal ); 
				
				var URL = this.m_Host + ":" + this.m_Port + i_URI; 
				
				AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Network::HTTPClient::POST() - debug - sending POST request to " + URL ); 
				AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::Network::HTTPClient::POST() - trace - request data " + i_Data ); 
				
				XMLHttp.open( "POST", URL, true ); 
				if( i_ContentType ) XMLHttp.setRequestHeader( "Content-type", i_ContentType );
				XMLHttp.send( i_Data ); 
				
			}; 
			
			this.__prototype__.PUT = function( i_URI, i_Data, i_ContentType ) {
				var XMLHttp = this.NewXMLHttp( this.m_PUTSignal ); 
				
				var URL = this.m_Host + ":" + this.m_Port + i_URI; 
				
				AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Network::HTTPClient::PUT() - debug - sending PUT request to " + URL ); 
				AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::Network::HTTPClient::POST() - trace - request data " + i_Data ); 
				
				XMLHttp.open( "PUT", URL, true ); 
				if( i_ContentType ) XMLHttp.setRequestHeader( "Content-type", i_ContentType ); 
				XMLHttp.send( i_Data ); 
				
			}; 
			
			this.__prototype__.DELETE = function( i_URI ) {
				var XMLHttp = this.NewXMLHttp( this.m_PUTSignal ); 
				
				var URL = this.m_Host + ":" + this.m_Port + i_URI; 
				
				AppKit.LOG( AppKit.LOG_SEVERITY.DEBUG, "AppKit::Network::HTTPClient::DELETE() - debug - sending DELETE request to " + URL ); 
				
				XMLHttp.open( "DELETE", URL, true ); 
				XMLHttp.send(); 
				
			}; 
			
			// Constructor
			{
				if( i_Host ) this.m_Host = i_Host; 
				else {
					this.m_Host = "http://" + window.location.hostname;
				}
				
				if( i_Port ) this.m_Port = i_Port; 
				else {
					this.m_Port = "80"; 
				}
				
			}
			
		}
		
		///////////////////////////////////////////////////////////////////////////////
		// WebSocketClient
		///////////////////////////////////////////////////////////////////////////////
		, WebSocketClient : function( i_Host, i_Port ) {
			this.__prototype__ = Object.getPrototypeOf( this ); 
			
			// Data Members
			Object.defineProperties(this, {
				m_Host : {
					value 		: ""
					, writable 	: true
				}
				, m_Port : {
					value 		: ""
					, writable	: true
				}
				
			}); 
			
			// Function Members			
			this.__prototype__.Host = function() {
				return m_Host; 
			}
			
			this.__prototype__.SetHost = function( i_Host ) {
				this.m_Host = (i_Host ? i_Host : ""); 
			}
			
			this.__prototype__.Port = function() {
				return m_Port; 
			}
			
			this.__prototype__.SetPort = function( i_Port ) {
				this.m_Port = (i_Port ? i_Port : ""); 
			}
			
			this.__prototype__.Send 	= function( i_Data ) {}; 
			this.__prototype__.Receive 	= function() {}; 
			
			// Constructor
			{
				if( i_Host ) this.SetHost( i_Host ); 
				if( i_Port ) this.SetPort( i_Port ); 
			}
			
		}
		
	}; 
	
}); 
