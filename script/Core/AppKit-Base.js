window.AppKit = {
	APPKIT_VERSION : 010000
	, LOG_SEVERITY : {
		TRACE		: 0x0
		, DEBUG		: 0x1
		, INFO		: 0x2
		, WARNING	: 0x3
		, ERROR		: 0x4
		, FATAL		: 0x5
	
	}
	, LOG_LEVEL : 0x3
	, LOG : function( Severity, Message, ConsoleFunction ){
		if( Severity > this.LOG_LEVEL - 1 ) {
			console.log( Message ); 
		}
	}
	, MakeID : function() {
		var ID = (new Date().getTime() + "-"); 
		
		var PossibleNumbers = "9078563412"; 
		
		for( var i = 0; i < 12; ++i )
			ID += PossibleNumbers.charAt( Math.floor( Math.random() * PossibleNumbers.length ) ); 
		
		return ID; 
		
	}
	
	, MakePosition : function( i_X, i_XUnit, i_Y, i_YUnit ) {
		var XCoordinate = (i_X && i_XUnit ? new AppKit.Coordinate( i_X, i_XUnit ) : null); 
		var YCoordinate = (i_Y && i_YUnit ? new AppKit.Coordinate( i_Y, i_YUnit ) : null); 

		return new AppKit.Position( XCoordinate, YCoordinate ); 
	}
	
	, MakeRelativePosition : function( i_X, i_XUnit, i_Y, i_YUnit ) {
		var XCoordinate = (i_X && i_XUnit ? new AppKit.Coordinate( i_X, i_XUnit ) : null); 
		var YCoordinate = (i_Y && i_YUnit ? new AppKit.Coordinate( i_Y, i_YUnit ) : null); 

		return new AppKit.RelativePosition( XCoordinate, YCoordinate ); 
	}
	
	, MakeAbsolutePosition : function( i_X, i_XUnit, i_Y, i_YUnit ) {
		var XCoordinate = (i_X && i_XUnit ? new AppKit.Coordinate( i_X, i_XUnit ) : null); 
		var YCoordinate = (i_Y && i_YUnit ? new AppKit.Coordinate( i_Y, i_YUnit ) : null); 

		return new AppKit.AbsolutePosition( XCoordinate, YCoordinate ); 
	}
	
	, MakeSize : function( i_1, i_2, i_3, i_4 ) {
		var XDistance = null; 
		var YDistance = null; 

		if( typeof i_1 == "object" || i_1 == null ) {
			if( i_1 ) {
				for( var Key in i_1 ) {
					XDistance = new AppKit.Distance( i_1[Key], Key ); 
				}
			}

			if( typeof i_2 == "object" || i_2 == null ) {
				for( var Key in i_2 ) {
					YDistance = new AppKit.Distance( i_2[Key], Key ); 
				}
			} else {
				if( i_2 && i_3 ) {
					YDistance = new AppKit.Distance( i_2, i_3 ); 
				}
			}

		} else {
			if( i_1 && i_2 )
				XDistance = new AppKit.Distance( i_1, i_2 ); 

			if( typeof i_3 == "object" || i_3 == null ) {
				for( var Key in i_3 ) {
					YDistance = new AppKit.Distance( i_3[Key], Key ); 
				}
			} else {
				if( i_3 && i_4 ) {
					YDistance = new AppKit.Distance( i_3, i_4 ); 
				}
			}
		}

		return new AppKit.Size( XDistance, YDistance ); 

	}
	
}; 

define(function(){}); 
