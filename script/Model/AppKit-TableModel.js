define( ["AppKit/Core/AppKit-Base", "AppKit/Model/AppKit-Model"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// TableModel : Model
	///////////////////////////////////////////////////////////////////////////////
	AppKit.TableModel = function( i_Rows ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Model.apply( this, [(i_Rows ? i_Rows : [])] ); 

		// Data Members
		Object.defineProperties(this, {
			m_Filter : {
				value 		: null
				, writable 	: true
			}
			, m_Sort : {
				value 		: null
				, writable 	: true
			}

		}); 
		
		// Function Members & Inheritance
		this.__prototype__.SetFilter = function( i_Filter, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			this.m_Filter = i_Filter; 

			if( i_DoEmitRefresh )
				this.m_RefreshViewSignal.Emit( [this, (i_AllowRecursiveEmitRefresh === false ? false : true)] ); 

		}; 

		this.__prototype__.SetSort = function( i_Sort, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			this.m_Sort = i_Sort; 

			if( i_DoEmitRefresh )
				this.m_RefreshViewSignal.Emit( [this, (i_AllowRecursiveEmitRefresh === false ? false : true)] ); 

		}; 

		this.__prototype__.NumRows = function( i_AllowRecursiveEmitRefresh ) {
			return this.m_Data.length; 
		}; 
		
		this.__prototype__.AddRow 		= function( i_Row, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) 	{}; 
		this.__prototype__.RemoveRow 	= function( i_Index, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {}; 
				
		this.__prototype__.SetValue = function( i_Value, i_RowIndex, i_ColumnIndex, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {}; 
		this.__prototype__.Value 	= function( i_RowIndex, i_ColumnIndex ) {
			return this.m_Data[i_RowIndex][i_ColumnIndex]; 
		}; 
		
		// Constructor
		{}
		
	}; 
	
	AppKit.TableModel.prototype = new AppKit.Model; 
	
}); 
