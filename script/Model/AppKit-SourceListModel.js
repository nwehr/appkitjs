define( ["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// SourceListModel
	///////////////////////////////////////////////////////////////////////////////
	AppKit.SourceListModel = function( i_Data ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		this.AppKit.Model.call( this );
		
		// Data Members
		Object.defineProperties(this, {
			m_Data : {
				value 		: []
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.Data = function() {
			return this.m_Data; 
		}; 
		
	}; 
	
	AppKit.SourceListModel.prototype = new AppKit.Model; 
	
}); 
