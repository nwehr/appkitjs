define( ["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/Model/AppKit-Model"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// GeneralTreeNode : Model
	///////////////////////////////////////////////////////////////////////////////
	AppKit.GeneralTreeNode = function( i_Value, i_Model ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Model : {
				value 		: null
				, writable 	: true
			}
			, m_Value : {
				value 		: []
				, writable 	: true
			}
			, m_Parent : {
				value 		: null
				, writable 	: true
			}
			, m_Child : {
				value 		: null
				, writable 	: true
			}
			, m_Sibling : {
				value 		: null
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetModel = function( i_Model ) {
			if( !i_Model || !(i_Model instanceof AppKit.GeneralTreeModel) ) {
				throw new Error( "parameter i_Node for AppKit::GeneralTreeNode::SetModel() is of incorrect type; expecting AppKit.GeneralTreeModel" ); 
			}
			
			this.m_Model = i_Model; 
			
		}; 
		
		this.__prototype__.Model = function() {
			return this.m_Model; 
		}; 
		
		this.__prototype__.SetValue = function( i_Value, i_DoEmitSignal ) {
			this.m_Value = i_Value; 
			
			if( this.m_Model && i_DoEmitSignal ) {
				this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
			}
			
		}; 
		
		this.__prototype__.SetValueAtIndex = function( i_Value, i_Index, i_DoEmitSignal ) {
			this.m_Value[i_Index] = i_Value; 
			
			if( this.m_Model && i_DoEmitSignal ) {
				this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
			}
			
		}; 
		
		this.__prototype__.Value = function() {
			return this.m_Value; 
			
		}; 
		
		this.__prototype__.ValueAtIndex = function( i_Index ) {
			return this.m_Value[i_Index]; 
		}; 
		
		this.__prototype__.SetValueAtIndex = function( i_Value, i_Index, i_DoEmitSignal ) {
			this.m_Value[i_Index] = i_Value; 
			
			if( this.m_Model && i_DoEmitSignal ) {
				this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
			}
			
		}; 
		
		this.__prototype__.ValueAtIndex = function( i_Index ) {
			return this.m_Value[i_Index]; 
		}; 
		
		this.__prototype__.SetParent = function( i_Node, i_DoEmitSignal ) {
			if( !i_Node || !(i_Node instanceof AppKit.GeneralTreeNode) ) {
				throw new Error( "parameter i_Node for AppKit::GeneralTreeNode::SetParent() is of incorrect type; expecting AppKit.GeneralTreeNode" ); 
			}
			
			this.m_Parent = i_Node; 
			
			if( this.m_Model && i_DoEmitSignal ) {
				this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
			}
			
			return this.m_Parent; 
			
		}; 
		
		this.__prototype__.Parent = function() {
			return this.m_Parent; 
		}; 
		
		this.__prototype__.SetChild = function( i_Node, i_DoEmitSignal ) {
			if( !i_Node || !(i_Node instanceof AppKit.GeneralTreeNode) ) {
				throw new Error( "parameter i_Node for AppKit::GeneralTreeNode::SetChild() is of incorrect type; expecting AppKit.GeneralTreeNode" ); 
			}
			
			if( this.m_Child ) {
				i_Node.SetSibling( this.m_Child, false ); 
			}
			
			this.m_Child = i_Node; 
			this.m_Child.SetParent( this, false ); 
			
			if( this.m_Model ) {
				this.m_Child.SetModel( this.m_Model )
				
				if( i_DoEmitSignal )
					this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
					
			}
			
			return this.m_Child; 
			
		}; 
		
		this.__prototype__.Child = function() {
			return this.m_Child; 
		}; 
		
		this.__prototype__.SetSibling = function( i_Node, i_DoEmitSignal ) {
			if( !i_Node || !(i_Node instanceof AppKit.GeneralTreeNode) ) {
				throw new Error( "parameter i_Node for AppKit::GeneralTreeNode::SetNextSibling() is of incorrect type; expecting AppKit.GeneralTreeNode" ); 
			}
			
			if( this.m_Sibling ) {
				i_Node.SetSibling( this.m_Sibling, false ); 
			}
			
			this.m_Sibling = i_Node; 
			
			if( this.m_Parent )
				this.m_Sibling.SetParent( this.m_Parent, false ); 
			
			if( this.m_Model ) {
				this.m_Sibling.SetModel( this.m_Model ); 
				
				if( i_DoEmitSignal )
					this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
					
			}
			
			return this.m_Sibling; 
			
		}; 
		
		this.__prototype__.Sibling = function() {
			return this.m_Sibling; 
		}; 
		
		this.__prototype__.Remove = function( i_DoEmitSignal ) {
			var PreviousNode 	= null; 
			var CurrentNode 	= null; 
			
			if( this.m_Parent ) {
				for( CurrentNode = this.m_Parent.Child(); CurrentNode != null; CurrentNode = CurrentNode.Sibling() ) {
					if( CurrentNode === this ) {
						if( PreviousNode ) {
							PreviousNode.m_Sibling = CurrentNode.Sibling(); 
						} else {
							this.m_Parent.m_Child = CurrentNode.Sibling(); 
						}
						
					}
					
					PreviousNode = CurrentNode; 
					
				}
				
				if( this.m_Model && i_DoEmitSignal )
					this.m_Model.RefreshViewSignal().Emit( [this.m_Model, this] ); 
				
			}
			
		}; 
		
		this.__prototype__.IsLeaf = function() {
			return !(this.m_Child != null); 
		}; 
		
		this.__prototype__.Path = function( i_Path ) {
			var Path = i_Path ? i_Path : [];
			
			if( this.m_Parent ) {
				var Index = 0;
				
				for( var Child = this.m_Parent.Child(); Child !== null; Child = Child.Sibling() ) {
					if( Child === this ) {
						Path.unshift( Index );
						Path = this.m_Parent.Path( Path ); 
					}
					
					++Index;
					
				}
			}
			
			return Path;
			
		};
		
		// Constructor
		{
			if( i_Model ) this.SetModel( i_Model ); 
			if( i_Value ) this.SetValue( i_Value ); 
		}
		
	}; 
	
	///////////////////////////////////////////////////////////////////////////////
	// GeneralTreeModel : Model
	///////////////////////////////////////////////////////////////////////////////
	AppKit.GeneralTreeModel = function( i_Root ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Model.apply( this, [i_Root ? i_Root : new AppKit.GeneralTreeNode( "Root", this )] );  
		
		// Data Members
		Object.defineProperties(this, {}); 
		
		// Function Members
		this.__prototype__.Log = function( i_Root, i_Tabs ) {
			var Tabs = i_Tabs ? i_Tabs : ""; 
			
			console.log( Tabs + i_Root.Value() ); 
			
			if( !i_Root.IsLeaf() ) {
				for( var Root = i_Root.Child(); Root != null; Root = Root.Sibling() ) {
					this.Log( Root, Tabs + "\t" ); 
				}
				
			}
			
		}; 
		
		this.__prototype__.GetNode = function( i_Path, i_Root ) {
			var Root = i_Root ? i_Root.Child() : this.m_Data.Child(); 
			
			for( i = 0; i < i_Path[0]; ++i ) {
				Root = Root.Sibling(); 
			}
			
			if( i_Path.length > 1 ) {
				i_Path.shift(); 
				Root = this.GetNode( i_Path, Root ); 
			}
				
			return Root; 
			
		}; 
		
		// Constructor
		{}
		
	}; 
	
	AppKit.GeneralTreeModel.prototype = new AppKit.Model; 
	
}); 
