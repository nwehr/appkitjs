define( ["AppKit/Core/AppKit-Base", "AppKit/Model/AppKit-Model"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// WebSQLTableModel : TableModel
	///////////////////////////////////////////////////////////////////////////////
	AppKit.WebSQLTableModel = function( i_Name, i_Size ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Model.apply( this, [[]] ); 

		// Data Members
		Object.defineProperties(this, {
			m_Database : {
				value 		: null
				, writable 	: true
			}
		}); 
		
		// Function Members
		this.__prototype__.Connect = function( i_Name, i_Size ) {
			this.m_Database = openDatabase( i_Name, "1.0", "AppKit Database", (i_Size ? i_Size : 2 * 1024 * 1024) );
		}; 

		// Constructor
		{
			if( i_Name ) this.Connect( i_Name, i_Size ); 
		}
		
	}; 
	
	AppKit.WebSQLTableModel.prototype = new AppKit.TableModel; 
		
}); 
