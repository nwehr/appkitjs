define( ["AppKit/Core/AppKit-Base", "AppKit/Model/AppKit-Model"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// SimpleTableModel : SimpleModel
	///////////////////////////////////////////////////////////////////////////////
	AppKit.SimpleTableModel = function( i_Rows ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.TableModel.apply( this, [(i_Rows ? i_Rows : [])] ); 

		// Data Members
		Object.defineProperties(this, {
			m_FilteredData : {
				value 		: []
				, writable 	: true
			}

		}); 
		
		// Function Members & Inheritance
		this.__prototype__.RowSatisfiesFilter = function( i_Row ) {
			for( var Key in this.m_Filter ) {
				if( this.m_Filter[Key] instanceof RegExp ) {
					if( !this.m_Filter[Key].test( i_Row[Key] ) )
						return false; 

				} else {
					if( i_Row[Key] != this.m_Filter[Key] )
						return false; 

				}

			}

			return true; 

		}; 

		this.__prototype__.SetFilter = function( i_Filter, i_DoEmitSignal ) {
			AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::TableModel::SetFilter() - trace - " + (i_Filter ? "setting" : "removing") + " filter" ); 

			this.m_Filter 		= i_Filter; 
			this.m_FilteredData = []; 

			for( var i = 0; i < this.m_Data.length; ++i ) {
				if( this.RowSatisfiesFilter( this.m_Data[i] ) )
					this.m_FilteredData.push( this.m_Data[i] ); 

			}

			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 

		}; 

		this.__prototype__.SortCompare = function( a, b ) {
			for( var Key in this.m_Sort ) {
				if( this.m_Sort[Key] == "asc" ) {
					if( a[Key] < b[Key] )
						return -1; 

					if( a[Key] > b[Key] )
						return 1; 

				}

				if( this.m_Sort[Key] == "desc" ) {
					if( a[Key] < b[Key] )
						return 1; 

					if( a[Key] > b[Key] )
						return -1; 
					
				}

			}

			return 0; 

		}; 

		this.__prototype__.SetSort = function( i_Sort, i_DoEmitSignal ) {
			this.m_Sort = i_Sort; 
			this.m_Data.sort( this.SortCompare.bind(this) ); 

			if( this.m_Filter )
				this.m_Data.sort( this.SortCompare.bind(this) ); 

			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 

		}; 

		this.__prototype__.NumRows = function() {
			if( this.m_Filter )
				return this.m_FilteredData.length; 

			return this.m_Data.length; 
		}; 
		
		this.__prototype__.AddRow = function( i_Row, i_DoEmitSignal ) {
			AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::TableModel::AddRow() - trace - adding row at " + this.m_Data.length ); 
			
			this.m_Data.push( i_Row ); 
			
			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 
			
		}; 
		
		this.__prototype__.RemoveRow = function( i_Index, i_DoEmitSignal ) {
			AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::TableModel::RemoveRow() - trace - removing row " + i_Index ); 
			
			this.m_Data.splice( i_Index, 1 ); 
			
			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 
			
		}; 
		
		this.__prototype__.Value = function( i_RowIndex, i_ColumnIndex ) {
			if( this.m_Filter )
				return this.m_FilteredData[i_RowIndex][i_ColumnIndex]; 

			return this.m_Data[i_RowIndex][i_ColumnIndex]; 

		}; 
		
		this.__prototype__.SetValue = function( i_Value, i_RowIndex, i_ColumnIndex, i_DoEmitSignal ) {
			AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::TableModel::SetValue() - trace - setting value at row index " + i_RowIndex + " and column index " + i_ColumnIndex ); 

			this.m_Data[i_RowIndex][i_ColumnIndex ? i_ColumnIndex : 0] = i_Value; 
			
			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 
			
		}; 
		
		// Constructor
		{
			if( i_Rows ) this.m_Data = i_Rows;
		}
		
	}; 
	
	AppKit.SimpleTableModel.prototype = new AppKit.Model; 
		
}); 
