define( ["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot", "AppKit/Model/AppKit-Model"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// RadioModel : Model
	///////////////////////////////////////////////////////////////////////////////
	AppKit.RadioModel = function( i_Key, i_Value, i_IsSelected ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.Model.apply( this, [] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Key : {
				value 		: ""
				, writable 	: true
			}
			, m_Value : {
				value 		: ""
				, writable 	: true
			}
			, m_IsSelected : {
				value 		: false
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetIsSelected = function( i_IsSelected, i_DoEmitSignal ) {
			this.m_IsSelected = i_IsSelected; 
			
			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 
			
		}; 
		
		this.__prototype__.IsSelected = function() {
			return this.m_IsSelected; 
		}; 
		
		this.__prototype__.SetKey = function( i_Key, i_DoEmitSignal ) {
			this.m_Key = i_Key; 
			
			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 
				
		}; 
		
		this.__prototype__.Key = function() {
			return this.m_Key; 
		}; 
		
		this.__prototype__.SetValue = function( i_Value, i_DoEmitSignal ) {
			this.m_Value = i_Value; 
			
			if( i_DoEmitSignal )
				this.m_RefreshViewSignal.Emit( [this] ); 
			
		}; 
		
		// Constructor
		{
			if( i_Key ) this.SetKey( i_Key, false ); 
			if( i_Value ) this.SetValue( i_Value, false ); 
			if( i_IsSelected) this.SetIsSelected( i_IsSelected, false ); 
		}
		
	}; 
	
	AppKit.RadioModel.prototype = new AppKit.Model; 
	
}); 
