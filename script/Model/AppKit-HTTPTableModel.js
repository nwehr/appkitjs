define( ["AppKit/Core/AppKit-Base", "AppKit/Model/AppKit-TableModel"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// HTTPTableModel : TableModel
	///////////////////////////////////////////////////////////////////////////////
	AppKit.HTTPTableModel = function( i_HTTPClient, i_URI ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		AppKit.TableModel.apply( this, [[]] ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_HTTPClient : {
				value 		: null
				, writable	: true
			}
			, m_URI	: {
				value 		: ""
				, writable 	: true
			}
			, m_HTTPRequestSlot : {
				value 		: new AppKit.Slot( null, this )
				, writable 	: true
			}
			, m_RemoteNumRows : {
				value 		: 0
				, writable 	: true
			}
			, m_Events : {
				value 		: []
				, writable 	: true
			}
			, m_AppendData : {
				value 		: true
				, writable 	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.HandleHTTPRequest = function( i_Args ) {
			var json = JSON.parse( i_Args[0].responseText ); 

			if( json.status == "OK" ) {
				AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::HTTPTableModel::HTTPRequestHandler() - trace - response with event_id " + json.event_id ); 
				
				for( var i = 0; i < this.m_Events.length; ++i ) {
					if( json.event_id == this.m_Events[i].event_id ) {
						this.m_Events[i].selector.apply( this, [json.data, this.m_Events[i].arg_refresh, this.m_Events[i].arg_allowrefresh] ); 
						this.m_Events.splice( i, 1 ); 
						
					}
					
				}
				
			} else {
				AppKit.LOG( AppKit.LOG_SEVERITY.ERROR, "AppKit::HTTPTableModel::HTTPRequestHandler() - error - server error: " + json.message ); 
				throw new Error( json.message ); 
			}
			
		}; 

		this.__prototype__.SetHTTPClient = function( i_HTTPClient ) {
			if( !i_HTTPClient || !(i_HTTPClient instanceof AppKit.Network.HTTPClient) )
				throw new Error( "parameter i_HTTPClient for AppKit::HTTPTableModel::SetHTTPClient() is of incorrect type; expecting AppKit.Network.HTTPClient" ); 
			
			this.m_HTTPClient = i_HTTPClient; 
			
		}; 
		
		this.__prototype__.SetURI = function( i_URI ) {
			this.m_URI = i_URI; 
		}; 
		
		this.__prototype__.HandleSetValue = function( i_Data, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			if( i_DoEmitRefresh )
				this.m_RefreshViewSignal.Emit( [this, (i_AllowRecursiveEmitRefresh === false ? false : true)] ); 
			
		}; 

		this.__prototype__.SetValue = function( i_Value, i_RowIndex, i_ColumnIndex, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			this.m_Data[i_RowIndex][i_ColumnIndex] = i_Value; 

			var EventID = AppKit.MakeID(); 
			this.m_Events.push( {event_id:EventID, selector:this.HandleSetValue, arg_refresh:i_DoEmitRefresh, arg_allowrefresh: i_AllowRecursiveEmitRefresh} ); 
			
			var Parameters = ("?cmd=set_value&event_id=" + EventID + "&value=" + i_Value); 
			
			Parameters += ("&row_index=" + (i_RowIndex ? i_RowIndex : 0)); 
			Parameters += ("&col_index=" + (i_ColumnIndex ? i_ColumnIndex : 0)); 
			
			if( this.m_Filter ) {
				for( var Key in this.m_Filter ) {
					Parameters += "&filter[" + encodeURIComponent( Key ) + "]=" + encodeURIComponent( this.m_Filter[Key] ); 
				}
			}

			if( this.m_Sort ) {
				for( var Key in this.m_Sort ) {
					Parameters += "&sort[" + encodeURIComponent( Key ) + "]=" + encodeURIComponent( this.m_Sort[Key] ); 
				}
			}
			
			this.m_HTTPClient.GET( this.m_URI + Parameters ); 
			
			return EventID; 
			
		}; 

		this.__prototype__.HandleFetchRemoteRows = function( i_Data, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			if( i_Data.length ) {
				this.m_Data = (this.m_AppendData ? this.m_Data.concat( i_Data ) : i_Data); 

				if( i_DoEmitRefresh )
					this.m_RefreshViewSignal.Emit( [this, (i_AllowRecursiveEmitRefresh === false ? false : true)] ); 

			}
		
		}; 
		
		this.__prototype__.FetchRemoteRows = function( i_StartIndex, i_EndIndex, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			if( i_StartIndex == 0 )
				this.m_AppendData = false; 

			var EventID 	= AppKit.MakeID(); 

			this.m_Events.push( {event_id:EventID, selector:this.HandleFetchRemoteRows, arg_refresh: i_DoEmitRefresh, arg_allowrefresh: i_AllowRecursiveEmitRefresh} ); 

			var StartIndex 	= (i_StartIndex != undefined && i_StartIndex < this.m_RemoteNumRows && i_StartIndex > -1 ? i_StartIndex : 0);
			var EndIndex 	= (i_EndIndex != undefined && i_EndIndex < this.m_RemoteNumRows && i_EndIndex > -1 ? i_EndIndex : this.m_RemoteNumRows - 1);

			var Parameters = ("?cmd=rows&event_id=" + EventID); 
			
			Parameters += ("&start_index=" + StartIndex); 
			Parameters += ("&end_index=" + EndIndex); 

			if( this.m_Filter ) {
				for( var Key in this.m_Filter ) {
					Parameters += "&filter[" + encodeURIComponent( Key ) + "]=" + encodeURIComponent( this.m_Filter[Key] ); 
				}
			}

			if( this.m_Sort ) {
				for( var Key in this.m_Sort ) {
					Parameters += "&sort[" + encodeURIComponent( Key ) + "]=" + encodeURIComponent( this.m_Sort[Key] ); 
				}
			}

			this.m_HTTPClient.GET( this.m_URI + Parameters ); 
			
			return EventID; 
			
		}; 

		this.__prototype__.HandleFetchRemoteNumRows = function( i_Data, i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			this.m_RemoteNumRows = i_Data; 
			this.FetchRemoteRows( 0 , (this.m_RemoteNumRows < 26 ? this.m_RemoteNumRows - 1 : 24), i_DoEmitRefresh, false ); 
		}; 

		this.__prototype__.FetchRemoteNumRows = function( i_DoEmitRefresh, i_AllowRecursiveEmitRefresh ) {
			var EventID = AppKit.MakeID(); 
			AppKit.LOG( AppKit.LOG_SEVERITY.TRACE, "AppKit::HTTPTableModel::FetchRemoteNumRows() - trace - request with event_id " + EventID ); 

			this.m_Events.push( {event_id:EventID, selector:this.HandleFetchRemoteNumRows, arg_refresh: i_DoEmitRefresh, arg_allowrefresh: i_AllowRecursiveEmitRefresh} ); 

			var Parameters = "?cmd=num_rows&event_id=" + EventID;

			if( this.m_Filter ) {
				for( var Key in this.m_Filter ) {
					Parameters += "&filter[" + encodeURIComponent( Key ) + "]=" + encodeURIComponent( this.m_Filter[Key] ); 
				}
			}

			if( this.m_Sort ) {
				for( var Key in this.m_Sort ) {
					Parameters += "&sort[" + encodeURIComponent( Key ) + "]=" + encodeURIComponent( this.m_Sort[Key] ); 
				}
			}

			this.m_HTTPClient.GET( this.m_URI + Parameters ); 
			
			return EventID; 
			
		}; 

		this.__prototype__.RemoteNumRows = function() {
			return this.m_RemoteNumRows; 
		}; 
		
		this.__prototype__.NumRows = function( i_AllowRecursiveEmitRefresh ) {
			if( i_AllowRecursiveEmitRefresh !== false )
				this.FetchRemoteNumRows( true, false ); 

			return this.m_Data.length; 
			
		}; 
				
		// Constructor
		{
			if( i_HTTPClient ) this.SetHTTPClient( i_HTTPClient );  
			if( i_URI ) this.SetURI( i_URI ); 
			
			if( this.m_HTTPClient ) {
				this.m_HTTPRequestSlot.SetSelector( this.HandleHTTPRequest ); 
				this.m_HTTPClient.GETSignal().Connect( this.m_HTTPRequestSlot ); 
				
			}

		}
		
	}; 
	
	AppKit.HTTPTableModel.prototype = new AppKit.TableModel; 
	
}); 
