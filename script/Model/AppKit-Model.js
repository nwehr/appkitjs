define( ["AppKit/Core/AppKit-Base", "AppKit/Core/AppKit-SignalSlot"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Model
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Model = function( i_Data ) {
		this.__prototype__ = Object.getPrototypeOf( this ); 
		
		// Data Members
		Object.defineProperties(this, {
			m_Data : {
				value 		: i_Data
				, writable 	: true
			}
			, m_RefreshViewSignal : {
				value 		: new AppKit.Signal()
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.SetData = function( i_Data ) {
			this.m_Data = i_Data; 
		}; 
		
		this.__prototype__.Data = function() {
			return this.m_Data; 
		}; 
		
		this.__prototype__.RefreshViewSignal = function() {
			return this.m_RefreshViewSignal; 
		}; 
		
		// Constructor 
		{}
		
	}; 
	
}); 
