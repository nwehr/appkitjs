
define(["AppKit/Core/AppKit-Base", "AppKit/View/AppKit-Window"], function(){
	///////////////////////////////////////////////////////////////////////////////
	// Application
	///////////////////////////////////////////////////////////////////////////////
	AppKit.Application = function( i_Title, i_ParentElement ) {
		this.__prototype__ = Object.getPrototypeOf( this );
		
		// Data Members
		Object.defineProperties(this, {
			m_MainWindow : {
				value		: new AppKit.Window( "Main Window", 10000 )
				, writable	: true
			}
			
		}); 
		
		// Function Members
		this.__prototype__.MainWindow = function() {
			return this.m_MainWindow; 
		}; 
		
		// Constructor
		{
			this.m_MainWindow.TitleBar().SetTitle( i_Title ); 
			this.m_MainWindow.AddClass( "AppKit-MainWindow" ); 
			
			this.m_MainWindow.TitleBar().Hide(); 
			
/*
			this.m_MainWindow.TitleBar().SetLHSelectors( [new AppKit.Button("Close")] ); 
			this.m_MainWindow.TitleBar().SetRHSelectors( [new AppKit.Button("Okay"), new AppKit.Button("Cancel")] ); 
*/
			
			var ParentElement = (i_ParentElement ? i_ParentElement : document.body); 
			ParentElement.appendChild( this.m_MainWindow.Element() ); 
			
		}
		
	}; 
	
}); 
